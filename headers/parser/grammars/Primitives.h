
#ifndef DEF_PRIMITIVES
#define DEF_PRIMITIVES

#include <vector>

#include <boost/spirit/home/qi/nonterminal/rule.hpp>

#include "SkipGrammar.h"
#include "../tokens/GateLangTokens.h"

typedef std::string::const_iterator StringIterator;
typedef struct SkipGrammar CommentsSkipper;

namespace spirit = boost::spirit;
namespace qi = spirit::qi;

using namespace token;
using qi::grammar;
using qi::rule;


struct Primitives
{
    Primitives();

    rule<StringIterator, CommentsSkipper, std::string()> IDENTIFIER;
    rule<StringIterator, CommentsSkipper, int()> INTEGER;
    rule<StringIterator, CommentsSkipper, std::string()> gateIdentifier;

    // operators
    rule<StringIterator, CommentsSkipper>
        ASSIGN_OP, RIGHT_ARROW_OP, MEMBER_OP;

    // delimiters
    rule<StringIterator, CommentsSkipper>
        LEFT_BRACE, RIGHT_BRACE, LEFT_BRACKET,
        RIGHT_BRACKET, LEFT_PAREN, RIGHT_PAREN,
        START_TEMPLATE, END_TEMPLATE, LIST_SEP;

     // keywords
    rule<StringIterator, CommentsSkipper> keywords;

    // terminals
    rule<StringIterator, CommentsSkipper> GATE_TK, INCLUDE_TK, SELF_TK,
        AS_TK, PRIVATE_TK, PUBLIC_TK, MATCH_TK, ALIAS_TK, MOVE_TK, TEST_TK;

    rule<StringIterator, CommentsSkipper>
         GATE_KEYWORD, INCLUDE_KEYWORD, TEST_KEYWORD, SELF_KEYWORD,
        AS_KEYWORD, STATEMENT_SEP, ESCAPED_CHAR;

    rule<StringIterator, CommentsSkipper, RefactoringType()>
        ALIAS_KEYWORD, MATCH_KEYWORD, MOVE_KEYWORD;

    rule<StringIterator, CommentsSkipper, std::string()> QUOTED_STRING;
    rule<StringIterator, CommentsSkipper, std::string()> STRING;
    rule<StringIterator, CommentsSkipper, std::string()> DESTEX;
    rule<StringIterator, CommentsSkipper, std::string()> REGEX_BODY;
    rule<StringIterator, CommentsSkipper, std::string()> REGEX_OPTIONS;
    rule<StringIterator, CommentsSkipper, RegexToken()> regex;
    rule<StringIterator, CommentsSkipper, RegexOrStringToken()> regexOrString;
    rule<StringIterator, CommentsSkipper, bool()> BOOLEAN;
    rule<StringIterator, CommentsSkipper, VariableToken()> VARIABLE;
    rule<StringIterator, CommentsSkipper, int()> INDEX, TYPE_DECLARATION;
    rule<StringIterator, CommentsSkipper, ParameterToken()> variableDeclaration;
    rule<StringIterator, CommentsSkipper, std::vector<struct ParameterToken>()> paramsList;
    rule<StringIterator, CommentsSkipper, ModifierType()> MODIFIER;

};

#endif
