
#ifndef DEF_SKIPGRAMMAR
#define DEF_SKIPGRAMMAR

#include <boost/spirit/home/qi/nonterminal/rule.hpp>
#include <boost/spirit/home/qi/nonterminal/grammar.hpp>
#include <string>

typedef std::string::const_iterator StringIterator;

using boost::spirit::qi::grammar;
using boost::spirit::qi::rule;

struct SkipGrammar : public grammar<StringIterator>
{
    SkipGrammar();
    rule<StringIterator> skip;
};

#endif
