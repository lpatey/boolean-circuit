
#ifndef DEF_GATELANGGRAMMAR
#define DEF_GATELANGGRAMMAR

#include <vector>

#include <boost/spirit/home/qi/nonterminal/rule.hpp>
#include <boost/spirit/home/qi/nonterminal/grammar.hpp>

#include "SkipGrammar.h"
#include "Primitives.h"
#include "../tokens/GateLangTokens.h"

typedef std::string::const_iterator StringIterator;
typedef struct SkipGrammar CommentsSkipper;

namespace spirit = boost::spirit;
namespace qi = spirit::qi;

using namespace token;
using token::StatementToken;
using token::TemplateIdentifierToken;
using qi::grammar;
using qi::rule;


struct GateLangGrammar : public grammar<StringIterator,CommentsSkipper, std::vector<StatementToken>() >
{
    GateLangGrammar();

    // start rule
    rule<StringIterator, CommentsSkipper, std::vector<StatementToken>() > context;
    rule<StringIterator, CommentsSkipper, TemplateIdentifierToken()> templateIdentifier;

    rule<StringIterator, CommentsSkipper, GatePrototypeToken()> gatePrototype;
    rule<StringIterator, CommentsSkipper, MatchResultToken()> matchResult;
    rule<StringIterator, CommentsSkipper, MatchValueToken()> matchValue;
    rule<StringIterator, CommentsSkipper, MatchValueTupleToken()> matchValueTuple;
    rule<StringIterator, CommentsSkipper, std::vector<MatchResultToken>()> matchResultTuple;

    rule<StringIterator, CommentsSkipper, std::vector<VariableToken>()> variableTuple;
    rule<StringIterator, CommentsSkipper, std::vector<bool>()> booleanTuple;
    rule<StringIterator, CommentsSkipper, std::vector<OptionalVariableToken>()> optionalVariableTuple;
    rule<StringIterator, CommentsSkipper, std::vector<TemplateParameterToken>()> templateTuple;
    rule<StringIterator, CommentsSkipper, std::vector<int>()> templateValueTuple;

    rule<StringIterator, CommentsSkipper, GateCallToken()> gateCall;
    rule<StringIterator, CommentsSkipper, ConditionMatchToken()> conditionMatch;
    rule<StringIterator, CommentsSkipper, OptionalVariableToken()> optionalVariable;
    rule<StringIterator, CommentsSkipper, AssignmentToken()> assignmentExpression;
    rule<StringIterator, CommentsSkipper, TupleExpressionToken()> tupleExpression;
    rule<StringIterator, CommentsSkipper, InstructionToken()> instruction;
    rule<StringIterator, CommentsSkipper, StatementToken()> statement;
    rule<StringIterator, CommentsSkipper, GateDeclarationToken()> gateDeclaration;
    rule<StringIterator, CommentsSkipper, TemplateParameterToken()> templateParameter;
    rule<StringIterator, CommentsSkipper, RefactoringPrimitiveToken()> refactoringPrimitive;
    rule<StringIterator, CommentsSkipper, std::vector<RefactoringPrimitiveToken>()> refactoringList;
    rule<StringIterator, CommentsSkipper, RefactoringStatementToken()> refactoringGroup;
    rule<StringIterator, CommentsSkipper, RefactoringStatementToken()> refactoringStatement;
    rule<StringIterator, CommentsSkipper, IncludeStatementToken()> includeStatement;
    rule<StringIterator, CommentsSkipper, LabelledPrimitiveToken()> labelledPrimitive;
    rule<StringIterator, CommentsSkipper, std::vector<LabelledPrimitiveToken>()> labelledTuple;
    rule<StringIterator, CommentsSkipper, TestMatchToken()> testMatch;
    rule<StringIterator, CommentsSkipper, TestStatementToken()> testStatement;

    Primitives p;

    bool parseTemplate(std::string, TemplateIdentifierToken&);

};

#endif
