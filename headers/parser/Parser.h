
#ifndef DEF_PARSER
#define DEF_PARSER

#include "grammars/SkipGrammar.h"
#include "grammars/GateLangGrammar.h"
#include "tokens/GateLangTokens.h"
#include <string>

#include <boost/spirit/home/qi/nonterminal/grammar.hpp>

typedef std::string::const_iterator StringIterator;
typedef struct SkipGrammar CommentsSkipper;
typedef grammar<StringIterator, CommentsSkipper, std::vector<token::StatementToken>() > GateLibAbstractGrammar;

using boost::spirit::qi::grammar;

class Parser {

    private:
    GateLangGrammar& myGrammar;

    public:
    Parser(GateLangGrammar&);
    bool parse(std::string,std::vector<token::StatementToken>&);

};

#endif
