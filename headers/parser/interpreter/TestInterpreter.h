
#ifndef DEF_TESTINTERPRETER
#define DEF_TESTINTERPRETER

#include "../tokens/GateLangTokens.h"
#include "../../core/Output.h"
#include "../../core/Input.h"
#include <map>
#include <string>

class FileContext;
class GateType;

class TestInterpreter : public Output, public Input {

    private :
    token::TestStatementToken test;
    FileContext* context;
    int index;
    GateType* type;

    public :

    TestInterpreter(token::TestStatementToken, FileContext*);

    void setNext(std::map<std::string,bool>);
    bool hasNext();
    std::map<std::string,bool> getNext() throw(GateLibException);
};

#endif
