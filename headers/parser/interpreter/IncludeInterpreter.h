
#ifndef DEF_INCLUDEINTERPRETER
#define DEF_INCLUDEINTERPRETER

#include "../tokens/GateLangTokens.h"
#include <boost/variant/apply_visitor.hpp>
#include <vector>

class FileContext;

class IncludeInterpreter : public boost::static_visitor< std::vector<FileContext*> > {

    public :

    IncludeInterpreter(token::IncludeStatementToken, FileContext*);

    std::vector<FileContext*> operator()(const token::RegexToken&);
    std::vector<FileContext*> operator()(const std::string&);

};

#endif
