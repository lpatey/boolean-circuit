#ifndef DEF_STATEMENTINTERPRETER
#define DEF_STATEMENTINTERPRETER

#include "../tokens/GateLangTokens.h"
#include <boost/variant/apply_visitor.hpp>

class FileContext;

class StatementInterpreter : public boost::static_visitor<void> {

    private :

    FileContext* context;

    public :

    StatementInterpreter(FileContext*);

    void operator()(token::GateDeclarationToken&);
    void operator()(token::IncludeStatementToken&);
    void operator()(token::RefactoringStatementToken&);
    void operator()(token::TestStatementToken&);

};

#endif
