
#ifndef DEF_STATICGATEINTERPRETER
#define DEF_STATICGATEINTERPRETER

#include "../tokens/GateLangTokens.h"
#include <boost/variant/apply_visitor.hpp>
#include <vector>
#include <set>

class FileContext;
class StaticGateType;

class StaticGateInterpreter : public boost::static_visitor<void> {

    private :
    StaticGateType* gateType;
    token::GateDeclarationToken& declaration;

    void computeEachValueMatching(std::set<std::string>, std::map<std::string,bool>, token::MatchValueTupleToken&,
        std::vector<token::MatchResultToken>&);

    public :

    StaticGateInterpreter(token::GateDeclarationToken&, FileContext*);

    void operator()(token::AssignmentToken&);
    void operator()(token::ConditionMatchToken&);
    StaticGateType* getGateType();
};

#endif
