
#ifndef DEF_VARIABLE
#define DEF_VARIABLE

#include "../tokens/GateLangTokens.h"
#include <vector>
#include <string>
#include <utility>

class EdgeStructure;
class Variable;
class GateType;
class DynamicGateType;

class Variable {

    private :
    std::vector<std::pair<std::string,EdgeStructure*> > edges;

    public :
    Variable();

    void add(std::string,EdgeStructure*);
    void add(Variable&);
    void add(GateType*,DynamicGateType*);
    void add(token::VariableToken&,Variable&);
    void add(EdgeStructure*);
    std::vector<std::pair<std::string,EdgeStructure*> >& getEdges();

    void set(token::VariableToken&,EdgeStructure*);
    EdgeStructure* get(token::VariableToken&);
    EdgeStructure* get(std::string);
};

#endif
