
#ifndef DEF_REFACTORINGINTERPRETER
#define DEF_REFACTORINGINTERPRETER

#include "../tokens/GateLangTokens.h"
#include <boost/variant/apply_visitor.hpp>
#include <map>
#include <string>

class FileContext;
class GateType;

class RefactoringInterpreter : public boost::static_visitor<void> {

    private :
    std::map<std::string,GateType*> types;
    token::RefactoringStatementToken refactoring;
    FileContext* context;

    public :

    RefactoringInterpreter(token::RefactoringStatementToken, FileContext*);

    void operator()(const token::RegexToken&, const std::vector<std::string>&);
    void operator()(const std::string&, const std::vector<std::string>&);

};

#endif
