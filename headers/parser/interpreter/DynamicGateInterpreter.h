
#ifndef DEF_DYNAMICGATEINTERPRETER
#define DEF_DYNAMICGATEINTERPRETER

#include "../tokens/GateLangTokens.h"
#include "../../core/dynamics/DynamicGateType.h"
#include "Variable.h"
#include <vector>
#include <map>
#include <string>
#include <boost/variant/apply_visitor.hpp>

class FileContext;

class DynamicGateInterpreter : public boost::static_visitor<bool> {

    private :
    DynamicGateType* gateType;
    std::map<std::string, Variable*> declaredVariables;
    std::vector<GateStructuresMapping> gateStructures;
    FileContext* context;
    token::GatePrototypeToken prototype;
    std::vector<token::InstructionToken> instructions;
    int instructionsSize;
    int currentIndex;


    void linkOutputs();
    void declareInputs();
    void declareLeftHandSide();
    void computeDeclarations(std::vector<token::OptionalVariableToken>&, Variable&);
    void computeRightHandSide();
    Variable computeLeftHandSideOfGateCall(token::GateCallToken&);
    void computeRightHandSideOfGateCall(token::GateCallToken&);
    void computeRightHandSideOfTuple(std::vector<token::VariableToken>&);

    public :

    DynamicGateInterpreter(token::GateDeclarationToken, FileContext*);

    bool operator()(token::AssignmentToken&);
    bool operator()(token::ConditionMatchToken&);

    DynamicGateType* getGateType();
};

#endif
