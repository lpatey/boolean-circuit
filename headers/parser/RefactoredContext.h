
#ifndef DEF_REFACTOREDCONTEXT
#define DEF_REFACTOREDCONTEXT

#include <boost/variant/apply_visitor.hpp>
#include "tokens/GateLangTokens.h"
#include "../core/Context.h"
#include <vector>
#include <string>
#include <set>
#include <map>

class GateType;
class FileContext;

class RefactoredContext : public Context, public boost::static_visitor<void> {

    private :
    FileContext* context;
    std::map<std::string,std::string> aliases;
    std::vector<token::RefactoringStatementToken> refactorings;
    token::RefactoringStatementToken currentRefactoring;

    void applyRefactoring(token::RefactoringStatementToken&);

    public :
    RefactoredContext(FileContext*, std::vector<token::RefactoringStatementToken>);

    bool hasType(std::string);
    GateType* getType(std::string) throw(GateTypeNotFoundException);
    std::set<std::string> getTypeNames();

    void operator()(const token::RegexToken&, const std::vector<std::string>&);
    void operator()(const std::string&, const std::vector<std::string>&);
};

#endif
