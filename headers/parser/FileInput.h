#ifndef DEF_FILEINPUT
#define DEF_FILEINPUT

#include "../core/Input.h"
#include "../core/exceptions/GateLibException.h"
#include <string>
#include <map>
#include <vector>
#include <fstream>

class FileInput : public Input
{
    private :
    std::string fileName;
    std::ifstream fileStream;
    std::vector<std::string> inputNames;

    void open() throw(GateLibException);

    public :

    FileInput(std::string);
    virtual ~FileInput();
    bool isOpened();
    bool hasNext();
    std::vector<std::string>& getInputNames();
    std::string getFileName() const;
    std::map<std::string,bool> getNext() throw(GateLibException);
};

#endif
