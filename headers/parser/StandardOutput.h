
#ifndef DEF_STANDARDOUTPUT
#define DEF_STANDARDOUTPUT

#include "../core/Output.h"

#include <string>
#include <map>

class StandardOutput : public Output
{
    public :

    void setNext(std::map<std::string,bool>);
};

#endif
