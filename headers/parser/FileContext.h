#ifndef DEF_FILECONTEXT
#define DEF_FILECONTEXT

#include "../core/DefaultContext.h"
#include "../core/exceptions/GateLibException.h"
#include "../core/exceptions/GateTypeNotFoundException.h"
#include "tokens/GateLangTokens.h"
#include <string>
#include <vector>
#include <map>
#include <utility>
#include <set>

class Context;
class GateType;
class FileContext;
class DynamicGateType;
class StaticGateType;
class GateStructure;

class FileContext : public DefaultContext
{
    private :
    std::vector<token::StatementToken> statements;
    static std::map<std::string,FileContext*> contexts;
    std::vector<Context*> subContexts;
    int subContextsSize;

    FileContext(std::string) throw(GateLibException);

    void compute ();

    DynamicGateType* createDynamicGateType(std::vector<std::string>,
        std::vector<std::string>, std::vector<token::InstructionToken>);

    StaticGateType* createStaticGateType(std::vector<std::string>,
        std::vector<std::string>, std::vector<token::InstructionToken>);

    public :

    static FileContext* getContext(std::string) throw(GateLibException);
    void addSubContext (Context*);
    void unsetType(std::string);
    std::map<std::string,GateType*> clearTypes();
    bool hasType(std::string);
    GateType* getType(std::string) throw(GateTypeNotFoundException);
    std::set<std::string> getTypeNames();
};

#endif
