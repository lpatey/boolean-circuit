
#ifndef DEF_GATELANGTOKENS
#define DEF_GATELANGTOKENS

#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/optional/optional.hpp>
#include <string>
#include <vector>
#include <map>

using boost::variant;
using boost::optional;

namespace token {
    enum ModifierType {
        PUBLIC,
        PRIVATE
    };

    enum ValueType {
        BOOLEAN,
        VARIABLE
    };

    enum TemplateParameterType {
        BOOLEAN_TPL,
        DECLARATION_TPL,
        UNDEFINED_TPL
    };

    enum RefactoringType {
        MATCH,
        ALIAS,
        MOVE
    };

    enum VariableCallType {
        NORMAL,
        INDEXED,
        MEMBERED
    };

    enum GateCallType {
        LIST,
        MAP
    };

    struct ParameterToken {
        std::string identifier;
        int dim;

        ParameterToken();
        std::vector<std::string> getNames();
    };

    typedef variant<
            ParameterToken,
            int,
            char
        > TemplateParameterToken;

    struct GatePrototypeToken {
        ModifierType modifier;
        std::string identifier;
        std::vector<ParameterToken> inputParams;
        std::vector<ParameterToken> outputParams;
        std::vector<TemplateParameterToken> templateParams;

        GatePrototypeToken();
        std::vector<std::string> paramsToNames(std::vector<ParameterToken>&);
        std::vector<std::string> getInputNames();
        std::vector<std::string> getOutputNames();
    };

    struct VariableToken {
        VariableCallType type;
        std::string identifier;
        std::string member;
        int index;

        std::string getName();
        VariableToken();
    };

    struct TestMatchToken {
        std::vector<bool> matches;
        std::vector<bool> values;
    };

    typedef variant<VariableToken,char> OptionalVariableToken;

    typedef variant<VariableToken,bool,char> MatchValueToken;

    typedef variant<VariableToken,bool> MatchResultToken;

    struct MatchValueTupleToken {
        std::vector<MatchValueToken> values;
    };

    struct RegexToken {
        std::string regex;
        std::string options;
    };

    struct LabelledPrimitiveToken {
        VariableToken name;
        VariableToken value;
    };

    struct GateCallToken {
        std::string identifier;
        std::vector<int> templateValueList;
        GateCallType type;
        std::vector<VariableToken> valueList;
        std::vector<LabelledPrimitiveToken> valueMap;
    };

    typedef variant<
            std::vector<VariableToken>,
            GateCallToken
        > TupleExpressionToken;

    struct ConditionMatchToken {
        std::vector<MatchValueTupleToken> matches;
        std::vector<MatchResultToken> results;
    };

    struct AssignmentToken {
        std::vector<OptionalVariableToken> variables;
        TupleExpressionToken values;
    };

    typedef boost::variant<
            ConditionMatchToken,
            AssignmentToken
        > InstructionToken;

    struct GateDeclarationToken {
        GatePrototypeToken prototype;
        std::vector<InstructionToken> instructions;
    };

    typedef variant<
            RegexToken,
            std::string
        > RegexOrStringToken;

    struct RefactoringPrimitiveToken {
        RegexOrStringToken identifier;
        std::vector<std::string> destinations;
    };

    struct RefactoringStatementToken {
        RefactoringType type;
        std::vector<RefactoringPrimitiveToken> aliases;
    };

    struct IncludeStatementToken {
        ModifierType modifier;
        RegexOrStringToken source;
        std::vector<RefactoringStatementToken> refactoringStatements;
    };

    struct TemplateIdentifierToken {
        std::string name;
        std::vector<int> params;

        std::string getName();
    };

    struct TestStatementToken {
        TemplateIdentifierToken identifier;
        std::vector<TestMatchToken> tests;
    };

    typedef boost::variant<
            GateDeclarationToken,
            RefactoringStatementToken,
            IncludeStatementToken,
            TestStatementToken
        > StatementToken;


};


BOOST_FUSION_ADAPT_STRUCT(
    token::RefactoringStatementToken,
    (token::RefactoringType, type)
    (std::vector<token::RefactoringPrimitiveToken>, aliases)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::IncludeStatementToken,
    (token::ModifierType, modifier)
    (token::RegexOrStringToken, source)
    (std::vector<token::RefactoringStatementToken>, refactoringStatements)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::ParameterToken,
    (std::string, identifier)
    (int, dim)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::VariableToken,
    (token::VariableCallType, type)
    (std::string, identifier)
    (std::string, member)
    (int, index)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::GateCallToken,
    (std::string, identifier)
    (std::vector<int>, templateValueList)
    (token::GateCallType, type)
    (std::vector<token::VariableToken>, valueList)
    (std::vector<token::LabelledPrimitiveToken>, valueMap)
)


BOOST_FUSION_ADAPT_STRUCT(
    token::MatchValueTupleToken,
    (std::vector<token::MatchValueToken>, values)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::ConditionMatchToken,
    (std::vector<token::MatchValueTupleToken>, matches)
    (std::vector<token::MatchResultToken>, results)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::GatePrototypeToken,
    (token::ModifierType, modifier)
    (std::string, identifier)
    (std::vector<token::ParameterToken>, inputParams)
    (std::vector<token::ParameterToken>, outputParams)
    (std::vector<token::TemplateParameterToken>, templateParams)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::RegexToken,
    (std::string, regex)
    (std::string, options)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::AssignmentToken,
    (std::vector<token::OptionalVariableToken>, variables)
    (token::TupleExpressionToken, values)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::GateDeclarationToken,
    (token::GatePrototypeToken, prototype)
    (std::vector<token::InstructionToken>, instructions)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::RefactoringPrimitiveToken,
    (token::RegexOrStringToken, identifier)
    (std::vector<std::string>, destinations)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::TestMatchToken,
    (std::vector<bool>, matches)
    (std::vector<bool>, values)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::TemplateIdentifierToken,
    (std::string, name)
    (std::vector<int>, params)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::TestStatementToken,
    (token::TemplateIdentifierToken, identifier)
    (std::vector<token::TestMatchToken>, tests)
)

BOOST_FUSION_ADAPT_STRUCT(
    token::LabelledPrimitiveToken,
    (token::VariableToken, name)
    (token::VariableToken, value)
)

#endif
