#ifndef DEF_EDGE
#define DEF_EDGE

#include <vector>
#include <string>
#include <utility>

class Gate;

class Edge
{
    private :

    bool value;
    std::vector<std::pair<std::string,Gate*> > outputs;

    public :

    Edge ();
    void addOutput (std::string, Gate*);
    std::vector<std::pair<std::string,Gate*> >& getOutputs();
    bool setValue (bool);
    bool getValue () const;
};

#endif
