
#ifndef DEF_OUTPUT
#define DEF_OUTPUT

#include <string>
#include <map>

class Output
{
    public :

    virtual void setNext(std::map<std::string,bool>) = 0;
};

#endif
