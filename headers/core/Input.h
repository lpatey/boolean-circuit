
#ifndef DEF_INPUT
#define DEF_INPUT

#include <string>
#include <map>
#include "exceptions/GateLibException.h"

class Input
{
    public :

    virtual bool hasNext() = 0;
    virtual std::map<std::string,bool> getNext() throw(GateLibException) = 0;
};

#endif
