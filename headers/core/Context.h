#ifndef DEF_CONTEXT
#define DEF_CONTEXT

#include "exceptions/GateTypeNotFoundException.h"
#include <map>
#include <set>
#include <string>

class GateType;

class Context
{
    public :

    Context ();
    virtual ~Context();
    virtual bool hasType(std::string) = 0;
    virtual GateType* getType(std::string) throw(GateTypeNotFoundException) = 0;
    virtual std::set<std::string> getTypeNames() = 0;
};


#endif
