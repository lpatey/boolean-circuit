#ifndef DEF_REGGATE
#define DEF_REGGATE

#include <map>
#include <string>

#include "MemoryBit.h"
#include "../Gate.h"

class RegGateType;

class RegGate : public Gate, MemoryBit
{
    private :

    std::map<std::string, MemoryBit*> memBits;

    public :

    RegGate ();
    RegGate (bool);

    static RegGateType* TYPE;
    GateType* getType() const;


    void handleSignal ();
    void tick(bool);

    std::map<std::string, MemoryBit*> getMemoryBits();
};



#endif
