#ifndef DEF_REGGATETYPE
#define DEF_REGGATETYPE

#include <string>

#include "../GateType.h"
#include "../GateContainer.h"

class RegGateType : public GateType
{
	private :
	GateContainer gateContainer;

    public :

    RegGateType ();
    ConnectionType connectionType (std::string,std::string);
    MetaType getMetaType ();
    GateContainer* getGateContainer ();
    void compute();
    int getMemoryBitsNumber();
};


#endif
