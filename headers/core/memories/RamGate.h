#ifndef DEF_RAMGATE
#define DEF_RAMGATE

#include <map>
#include <string>

#include "MemoryBit.h"
#include "../Gate.h"

class RamGateType;

class RamGate : public Gate
{
    private :

    std::map<std::string, MemoryBit*> memBits;
    RamGateType* type;

    public :

    RamGate(RamGateType*);
    ~RamGate();

    GateType* getType() const;

    void handleSignal ();
    void tick(bool);

    std::map<std::string, MemoryBit*> getMemoryBits();
};

#endif

