#ifndef DEF_MEMORYBIT
#define DEF_MEMORYBIT

class MemoryBit
{
    private :

    bool value;

    public :

    MemoryBit();
    MemoryBit(bool);

    bool getValue () const;
    void setValue (bool);
};



#endif
