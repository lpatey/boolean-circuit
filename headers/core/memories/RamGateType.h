#ifndef DEF_RAMGATETYPE
#define DEF_RAMGATETYPE

#include <string>

#include "../GateType.h"
#include "../GateContainer.h"

class RamGateType : public GateType
{
	private :
	GateContainer gateContainer;
	int size;
	int word;

    public :

    RamGateType (int, int, Context*);
    int getSize ();
    int getWord ();
    ConnectionType connectionType (std::string,std::string);
    MetaType getMetaType ();
    GateContainer* getGateContainer ();
    void compute();
    int getMemoryBitsNumber();
};


#endif

