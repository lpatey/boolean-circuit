
#ifndef DEF_COMBINATORYCYCLEEXCEPTION
#define DEF_COMBINATORYCYCLEEXCEPTION

#include "GateLibException.h"
#include <string>

class EdgeStructure;

class CombinatoryCycleException : public GateLibException
{
    private :
    EdgeStructure* edgeStructure;

    public :

    CombinatoryCycleException(EdgeStructure*);
    EdgeStructure* getEdgeStructure() const;
};

#endif

