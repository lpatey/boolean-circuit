
#ifndef DEF_TESTFAILEDEXCEPTION
#define DEF_TESTFAILEDEXCEPTION

#include "GateLibException.h"
#include "../../parser/tokens/GateLangTokens.h"
#include <string>
#include <map>

class TestFailedException : public GateLibException
{
    private :

    token::TestStatementToken test;
    std::map<std::string,bool> values;
    int index;


    public :

    TestFailedException(token::TestStatementToken,std::map<std::string,bool>,int);

    token::TestStatementToken& getTest();
    std::map<std::string,bool>& getValues();
    int getIndex();

};

#endif

