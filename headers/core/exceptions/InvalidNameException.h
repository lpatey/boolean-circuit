#ifndef DEF_INVALIDNAMEEXCEPTION
#define DEF_INVALIDNAMEEXCEPTION

#include "GateLibException.h"
#include <string>

class InvalidNameException : public GateLibException
{
    private :

    std::string name;

    public :

    InvalidNameException(std::string);
    std::string getName() const;

};

#endif

