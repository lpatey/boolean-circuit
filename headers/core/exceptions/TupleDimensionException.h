
#ifndef DEF_TUPLEDIMENSIONEXCEPTION
#define DEF_TUPLEDIMENSIONEXCEPTION

#include "GateLibException.h"
#include <string>

class TupleDimensionException : public GateLibException
{
    public :

    TupleDimensionException(std::string,std::string,std::string);

};

#endif

