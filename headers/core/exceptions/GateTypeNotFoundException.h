
#ifndef DEF_GATETYPENOTFOUNDEXCEPTION
#define DEF_GATETYPENOTFOUNDEXCEPTION

#include "GateLibException.h"
#include <string>

class GateTypeNotFoundException : public GateLibException
{
    private :
    std::string name;

    public :

    GateTypeNotFoundException(std::string);
    std::string getName() const;

};

#endif

