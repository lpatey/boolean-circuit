
#ifndef DEF_GATELIBEXCEPTION
#define DEF_GATELIBEXCEPTION

#include <string>

class GateLibException
{
    private :
    std::string message;

    public :

    GateLibException(std::string);
    std::string getMessage() const;
};

#endif

