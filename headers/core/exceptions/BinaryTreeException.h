
#ifndef DEF_BINARYTREEEXCEPTION
#define DEF_BINARYTREEEXCEPTION

#include "GateLibException.h"
#include <string>

class BinaryTreeException : public GateLibException
{
    public :
    BinaryTreeException(std::string);
};

#endif

