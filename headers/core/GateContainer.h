#ifndef DEF_GATECONTAINER
#define DEF_GATECONTAINER

#include "GateType.h" // Pour le MetaType
#include "DefaultContext.h"
#include "exceptions/GateLibException.h"

#include<vector>
#include<string>
#include<map>

class GateStructure;
class DynamicGateType;
class StaticGateType;

class GateContainer {
	private :
	std::vector<GateStructure*> gateStructures;
	// NB : les index du tableau correspondent aux id des GateStructure
	std::map<std::string, std::vector<GateStructure*> > internalInputs;
    std::map<std::string, GateStructure*> internalOutputs;
    std::vector<GateType*> usedTypes;
    DefaultContext context;

    public :
    GateContainer();
    ~GateContainer();

    std::vector<GateStructure*>& getGateStructures();

    std::vector<GateStructure*> getInternalInput (std::string);
    GateStructure* getInternalOutput (std::string);

    DynamicGateType* createDynamicGateType(std::vector<std::string>, std::vector<std::string>);
    StaticGateType* createStaticGateType(std::vector<std::string>, std::vector<std::string>);
    GateStructure* addGateStructure(GateType*);
    void addInput (std::string, GateStructure*);
    void setOutput (std::string, GateStructure*);
};

#endif
