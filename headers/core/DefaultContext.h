
#ifndef DEF_DEFAULTCONTEXT
#define DEF_DEFAULTCONTEXT

#include "exceptions/GateTypeNotFoundException.h"
#include "Context.h"
#include <map>
#include <set>
#include <string>

class GateType;

class DefaultContext : public Context
{
    protected :
    std::map<std::string, GateType*> types;

    public :

    DefaultContext ();
    virtual bool hasType(std::string);
    virtual GateType* getType(std::string) throw(GateTypeNotFoundException);
    bool setType(std::string,GateType*);
    std::set<std::string> getTypeNames();
};


#endif
