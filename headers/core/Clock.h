#ifndef DEF_CLOCK
#define DEF_CLOCK

#include <vector>
#include "Input.h"
#include "Output.h"

class Gate;
class Clock;

typedef void (*ClockListener) (Clock*);

class Clock
{
    private :

    bool running;
    int speed;
    Gate* root;
    int iterations;
    std::vector<ClockListener> listeners;

    public :

    Clock (Gate*);
    void addClockListener (ClockListener);
    bool isRunning ();
    int getSpeed ();
    int getIterations ();
    void setSpeed (int);
    Gate* getRoot();
    void start(Input& input, Output& output);
};

#endif
