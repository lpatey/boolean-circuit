
#ifndef DEF_EDGESTRUCTURE
#define DEF_EDGESTRUCTURE

#include "../exceptions/InvalidNameException.h"

#include <string>
#include <vector>
#include <utility>

class GateStructure;

class EdgeStructure
{
    private :
    std::pair<std::string,GateStructure*> input;
    std::vector<std::pair<std::string,GateStructure*> > outputs;
    int depth;

    public :

    EdgeStructure (std::pair<std::string,GateStructure*>);
    EdgeStructure ();

    void addOutput (std::string, GateStructure*) throw(InvalidNameException);
    std::vector<std::pair<std::string,GateStructure*> >& getOutputs();
    std::pair<std::string,GateStructure*>& getInput();
    void setDepth (int);
    int getDepth () const;
    bool hasInput() const;
};

#endif
