
#ifndef DEF_GATESTRUCTURE
#define DEF_GATESTRUCTURE

#include "../exceptions/InvalidNameException.h"

#include <string>
#include <map>
#include <utility>

class GateType;
class EdgeStructure;

class GateStructure
{
    private :
    GateType* type;
    int depth;
    int id;
    std::map<std::string,EdgeStructure*> inputs;
    std::map<std::string,EdgeStructure*> outputs;


    public :

    GateStructure (GateType*);
    ~GateStructure ();

    void setId (int);
    int getId () const;
    GateType* getType() const;
    EdgeStructure* getOutput (std::string);
    EdgeStructure* getInput (std::string);
    void addInput(std::string,EdgeStructure*) throw(InvalidNameException);
    std::map<std::string,EdgeStructure*>& getInputs();
    std::map<std::string,EdgeStructure*>& getOutputs();

    void setDepth (int);
    int getDepth () const;
};

#endif
