#ifndef DEF_STATICGATE
#define DEF_STATICGATE

#include "../Gate.h"
#include <map>
#include <string>

class GateType;
class StaticGateType;
class MemoryBit;

class StaticGate: public Gate {
	private :
	StaticGateType* type;
    std::map<std::string, MemoryBit*> memBits;

	public:
	StaticGate(StaticGateType*);
	GateType* getType() const;
	StaticGateType* getTrueType() const;
	std::map<std::string, MemoryBit*> getMemoryBits();
	void tick(bool);
};

#endif
