#ifndef DEF_STATICGATETYPE
#define DEF_STATICGATETYPE

#include "../tools/BinaryTree.h"
#include "../GateType.h"
#include "../GateContainer.h"
#include <string>
#include <vector>

class Context;

class StaticGateType: public GateType {
	private :

    BinaryTree truthTable;
	GateContainer gateContainer;
	std::map<std::pair<std::string, std::string>, ConnectionType> connectionMatrix;

	public:

	StaticGateType(std::vector<std::string>, std::vector<std::string>, Context*);
	MetaType getMetaType();

	BinaryTree& getTruthTable();
	void setTruthTable (BinaryTree);

    void declareConnected (std::string, std::string);
	ConnectionType connectionType(std::string, std::string);
	GateContainer* getGateContainer();
	void compute() throw (GateLibException);
    int getMemoryBitsNumber();
};

#endif
