#ifndef DEF_GATE
#define DEF_GATE

#include <map>
#include <string>

#include "exceptions/InvalidNameException.h"
#include "exceptions/GateLibException.h"

class GateType;
class Edge;
class RegGate;
class StaticGate;
class DynamicGate;
class MemoryBit;

class Gate
{
    protected :

    bool signal;
    std::map<std::string, Edge*> outputEdges;
    std::map<std::string, Edge*> inputEdges;

    public :
    Gate (GateType* argType);
    virtual ~Gate();

    void setInputEdge (std::string, Edge*) throw(InvalidNameException);

    Edge* getInputEdge (std::string);
    Edge* getOutputEdge (std::string);
    std::map<std::string, Edge*> getInputEdges ();
    std::map<std::string, Edge*> getOutputEdges ();

    virtual void handleSignal ();
    void clearSignal();
    bool hasSignal();

    virtual GateType* getType () const =0;
    static Gate* create (GateType*) throw (GateLibException);

    virtual std::map<std::string, MemoryBit*> getMemoryBits() =0;

    virtual void tick(bool) =0;
};

#endif
