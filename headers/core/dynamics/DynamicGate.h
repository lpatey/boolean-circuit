#ifndef DEF_DYNAMICGATE
#define DEF_DYNAMICGATE

#include <vector>
#include <map>
#include <string>
#include "../Gate.h"

class Edge;
class MemoryBit;
class EdgeStructure;
class DynamicGateType;
class GateStructure;

class DynamicGate : public Gate
{
    private :

    std::map<std::string, Edge*> internalInputEdges;
    std::map<std::string, Edge*> internalOutputEdges;
    std::map<std::string, MemoryBit*> memBits;
    std::vector<std::vector<Gate*> > subGates;
    DynamicGateType* type;

    // M�thodes appel�es par le constructeur
    void initSubGates ();
    void initInternalInputs ();
    void initInternalOutputs ();
    void initEdges ();
    void initMemBits ();
    void linkDirectEdges ();

    // M�thodes auxiliaires utilis�es par les initialisateurs
    void mapStructToEdges (EdgeStructure*, Edge*);
    Gate* getGateFromStruct (GateStructure*);

    // M�thodes utilis�es par tick()
    std::map<std::string, bool> compute (bool);
    std::map<std::string, bool> getEntries ();
    void addMemories (std::map<std::string, bool>*);
    void emitOutput (std::map<std::string,bool>);
    void refreshMemories (std::map<std::string,bool>);

    public :

    DynamicGate (DynamicGateType* type);
    ~DynamicGate ();

    Edge* getInternalInputEdge (std::string);
    Edge* getInternalOutputEdge (std::string);
    std::map<std::string, Edge*>& getInternalInputEdges ();
    std::map<std::string, Edge*>& getInternalOutputEdges ();
    std::map<std::string, MemoryBit*> getMemoryBits ();
    GateType* getType() const;

    void tick (bool);
};

#endif
