#ifndef DEF_DYNAMICGATETYPE
#define DEF_DYNAMICGATETYPE

#include "../GateType.h"
#include "../GateContainer.h"
#include "../tools/BinaryTree.h"
#include "../exceptions/InvalidNameException.h"
#include "../exceptions/GateLibException.h"
#include "../exceptions/CombinatoryCycleException.h"

#include <map>
#include <vector>
#include <utility>
#include <string>

class EdgeStructure;
class GateStructure;
class Context;
class GateType;

struct GateStructuresMapping {
    std::map<std::string,GateStructure*> outputs;
    std::map<std::string,std::vector<GateStructure*> > inputs;
};

class DynamicGateType : public GateType
{
    private :
    std::vector<GateStructure*> gateStructures;
    BinaryTree truthTable;
    bool cacheEnabled;
    GateContainer gateContainer;
    std::map<std::pair<std::string, std::string>, ConnectionType> connectionMatrix;
    std::map<std::string,EdgeStructure*> internalInputs;
    std::map<std::string, EdgeStructure*> internalOutputs;

    void computeGateStructureIds();


    public :

    DynamicGateType(std::vector<std::string>, std::vector<std::string>, Context*);
    ~DynamicGateType();

    GateStructuresMapping addGateStructure (GateType*);
    void addInternalOutput(EdgeStructure*,std::string) throw(InvalidNameException);
    std::vector<GateStructure*>& getGateStructures();
    int getMemoryBitsNumber();

    void compute() throw(CombinatoryCycleException, GateLibException);

    EdgeStructure* getInternalInput (std::string);
    std::map<std::string, EdgeStructure*>& getInternalInputs();
    EdgeStructure* getInternalOutput (std::string);
    std::map<std::string, EdgeStructure*>& getInternalOutputs();
    GateContainer* getGateContainer();

    BinaryTree& getTruthTable();
    bool isCacheEnabled() const;
    void setCacheEnabled (bool);

    void declareConnected (std::string, std::string);
    void declareCombinatory (std::string, std::string);
    ConnectionType connectionType (std::string, std::string);

    MetaType getMetaType();
};

#endif
