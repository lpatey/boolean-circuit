#ifndef DEF_GATETYPE
#define DEF_GATETYPE

#include <vector>
#include <string>

enum MetaType {
	STATIC, REG, DYNAMIC, RAM
};

enum ConnectionType {
	NONE, COMBINATORY, MEMORY
};

class Context;
class GateContainer;

class GateType
{
    protected :

    std::vector<std::string> inputNames;
    std::vector<std::string> outputNames;
    Context* context;
    bool finished;

    public :

    GateType (std::vector<std::string>, std::vector<std::string>, Context*);
    virtual ~GateType();

    std::vector<std::string> getInputNames () const;
    std::vector<std::string> getOutputNames () const;
    Context* getContext() const;
    virtual ConnectionType connectionType(std::string, std::string) = 0;
    virtual MetaType getMetaType() = 0;
    virtual void compute ();
    bool isFinished ();
    virtual GateContainer* getGateContainer () = 0;
    virtual int getMemoryBitsNumber() = 0;

};

#endif
