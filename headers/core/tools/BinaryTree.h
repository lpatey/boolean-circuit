#ifndef DEF_BINARYTREE
#define DEF_BINARYTREE

#include "../exceptions/BinaryTreeException.h"
#include <map>
#include <string>
#include <set>

class INode;

class BinaryTree
{
    private :

    INode* root;

    public :

    BinaryTree ();
    ~BinaryTree ();
    BinaryTree(const BinaryTree&);
    BinaryTree& operator=(const BinaryTree&);


    std::map<std::string,bool>& getValue( std::map<std::string, bool> );
	void setValue( std::map<std::string, bool>, std::map<std::string,bool>,bool=false) throw(BinaryTreeException);
	void copyExtract(std::set<std::string>, std::set<std::string>, BinaryTree*);

	bool isComplete ();
	std::string toString ();
};

#endif
