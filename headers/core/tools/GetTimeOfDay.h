#ifdef WIN32

#include <windows.h>

int gettimeofday(struct timeval *tv, struct timezone *tz);

#else

#include <sys/time.h>
#include <time.h>

#endif
