#ifndef DEF_BINARYNODE
#define DEF_BINARYNODE

#include "../exceptions/BinaryTreeException.h"
#include "INode.h"

#include <map>
#include <string>
#include <set>


class BinaryNode : public INode
{
    private :

    std::string name;
    INode* left;
    INode* right;

    INode* get (bool);
    void setLeaf (bool, std::map<std::string, bool>);
    void addNode (bool, std::string);

    public :

    BinaryNode (std::string);
    virtual ~BinaryNode();
    BinaryNode(const BinaryNode&);
    BinaryNode& operator=(const BinaryNode&);

    INode* clone() const;
    bool isLeaf() const;
    bool isNull() const;

    std::map<std::string,bool>& getValue( std::map<std::string, bool> );
    bool setValue( std::map<std::string, bool>, std::map<std::string,bool>,bool=false) throw(BinaryTreeException);

    void copyExtract(std::set<std::string>, std::set<std::string>,
    		std::map<std::string, bool>, BinaryTree*);

    bool isComplete ();
    std::string toString (std::string);
};

#endif
