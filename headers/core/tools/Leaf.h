#ifndef DEF_LEAF
#define DEF_LEAF

#include "../exceptions/BinaryTreeException.h"
#include "BinaryTree.h"
#include "INode.h"

#include <map>
#include <string>
#include <set>

class Leaf : public INode
{
    private :

    std::map<std::string, bool> values;

    public :

    Leaf();
    virtual ~Leaf();
    Leaf(std::map<std::string, bool>);
    INode* clone() const;
	bool isLeaf() const;
	bool isNull () const;
    std::map<std::string,bool>& getValue( std::map<std::string, bool> );
	bool setValue( std::map<std::string, bool>, std::map<std::string,bool>,bool=false) throw(BinaryTreeException);
	void copyExtract (std::set<std::string>, std::set<std::string>,
			std::map<std::string, bool>, BinaryTree*);
    bool isComplete ();
    std::string toString (std::string);
};

#endif
