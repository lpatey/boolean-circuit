#ifndef DEF_INODE
#define DEF_INODE

#include "../exceptions/BinaryTreeException.h"

#include <map>
#include <string>
#include <set>

class BinaryTree;

class INode
{
    public :

    virtual bool isLeaf() const = 0;
    virtual bool isNull() const = 0;
    virtual INode* clone() const = 0;
    virtual std::map<std::string,bool>& getValue( std::map<std::string, bool> ) = 0;
	virtual bool setValue( std::map<std::string, bool>, std::map<std::string,bool>,bool=false) throw(BinaryTreeException) = 0;
	virtual void copyExtract(std::set<std::string>, std::set<std::string>,
			std::map<std::string, bool>, BinaryTree*) = 0;
    virtual bool isComplete () = 0;
    virtual std::string toString (std::string) = 0;

};

#endif
