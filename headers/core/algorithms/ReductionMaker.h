#ifndef DEF_REDUCTIONMAKER
#define DEF_REDUCTIONMAKER

#include "../exceptions/GateLibException.h"
#include "../GateType.h"

#include <vector>
#include <string>
#include <map>

class StaticGateType;
class DynamicGateType;
class GateContainer;
class GateStructure;

class ReductionMaker {
	private :

	StaticGateType* sType;
	DynamicGateType* dType;
	MetaType metaType;

	// Redondant, mais permet de raccourcir (et optimiser) le code des m�thodes
	GateContainer* gateContainer;
	std::vector<std::string> iNames;
	std::vector<std::string> oNames;
	int iSize;
	int oSize;

    // Les vraies m�thodes appel�es en interne par compute
	bool computeStatic();
	bool computeDynamic();

	// Diverses sous-m�thodes
    bool isIrreductible (GateType*);
	void wrapIrreductibleGS (GateType*);
    void copyInternalInputs (DynamicGateType*, DynamicGateType*, std::map<GateStructure*,GateStructure*>);
    void copyInternalOutputs (DynamicGateType*, DynamicGateType*, std::map<GateStructure*,GateStructure*>);
    void connectIntoGateContainer (GateType* , std::vector<std::string>, int, std::vector<std::string>, int);

	public :

	ReductionMaker (StaticGateType*);
	ReductionMaker (DynamicGateType*);
	bool compute() throw(GateLibException);
};

#endif
