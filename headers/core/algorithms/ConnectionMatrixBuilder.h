#ifndef DEF_CONNECTIONMATRIXBUILDER
#define DEF_CONNECTIONMATRIXBUILDER

#include <set>
#include <string>

#include "../GateType.h"

class EdgeStructure;
class DynamicGateType;

class ConnectionMatrixBuilder
{
    private :

    std::set<std::pair<ConnectionType, EdgeStructure*> > todo;
    std::set<EdgeStructure*> connectedEdges;
    std::set<EdgeStructure*> combinatoryConnectedEdges;
    std::string currentInternalInputName;
	DynamicGateType* type;

    void computeNextSource();
    void computeNextEntry(GateType*, std::string, ConnectionType,
    		std::string, EdgeStructure*);

    public :

    ConnectionMatrixBuilder(DynamicGateType*);
    void compute();
    DynamicGateType* getType() const;
};

#endif
