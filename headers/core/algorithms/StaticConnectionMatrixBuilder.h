#ifndef DEF_STATICCONNECTIONMATRIXBUILDER
#define DEF_STATICCONNECTIONMATRIXBUILDER

#include <vector>
#include <string>
#include <map>

#include "../GateType.h"
#include "../tools/BinaryTree.h"

class StaticGateType;

class StaticConnectionMatrixBuilder
{
    private :

	StaticGateType* type;
	std::vector<std::string> iNames;
	std::vector<std::string> oNames;
	int iSize;
	int oSize;
	BinaryTree truthTable;

    bool areConnected(std::vector<std::string>,
            std::map<std::string,bool>, std::string, std::string );

    public :

    StaticConnectionMatrixBuilder(StaticGateType*);
    void compute();
    StaticGateType* getType() const;
};

#endif
