
#ifndef DEF_TOPOLOGICALSORTER
#define DEF_TOPOLOGICALSORTER

#include <set>

#include "../exceptions/CombinatoryCycleException.h"

class EdgeStructure;
class DynamicGateType;

class TopologicalSorter
{
    private :

    std::set<EdgeStructure*> todo;
	DynamicGateType* type;
    int depthThreshold;

    void computeNextEdge() throw(CombinatoryCycleException);
    void computeGateDepthsFromEdgeDepths();

    public :

    TopologicalSorter(DynamicGateType*);
    void compute() throw(CombinatoryCycleException);

    DynamicGateType* getType() const;
    int getDepthThreshold() const;
};

#endif
