#define VERSION "1.0.1"

#ifndef DEF_CONFIG
#define DEF_CONFIG

#include <boost/program_options.hpp>
#include <vector>
#include <string>

class Config;

class Config {
    private :

    static Config* config;
    boost::program_options::options_description cmdLineOptions;
    boost::program_options::options_description configFileOptions;
    boost::program_options::options_description visible;
    boost::program_options::variables_map vm;

    Config (int,char*[]);

    public :

    static Config* init(int, char*[]);
    static Config* getConfig();
    bool compute ();
    bool isComplete ();

    std::string getFilePath ();
    std::string getSimulatedGate ();
    std::string getInputFilePath ();
    std::vector<std::string> getIncludePaths ();
    int getCacheThreshold ();
    int getClockSpeed ();
    bool isVerboseEnabled ();
};

#endif
