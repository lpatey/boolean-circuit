# MakeFile pour le projet GateLib
# supporte le compilateur g++(gcc), requiert la librairie boost (dont la sous-librairie program_options compilée).

CPP=g++
CFLAGS=-Wall -I../../../../../Boost/include/
LDFLAGS=-L/users/09/info/morisset/Boost/lib/ -lboost_program_options -lboost_system -static
EXEC=GateLib
SRC=$(wildcard */*.cpp) $(wildcard */*/*.cpp) $(wildcard */*/*/*.cpp) $(wildcard */*.h) $(wildcard */*/*.h) $(wildcard */*/*/*.h)
OBJ= $(SRC:.cpp=.o)

all: $(EXEC)

GateLib: $(OBJ)
	$(CPP) -o $@ $^ $(LDFLAGS) $(CFLAGS)

%.o: %.cpp
	$(CPP) -o $@ -c $< $(CFLAGS)

.PHONY: clean mrproper

clean:
	rm -rf $(OBJ)

mrproper: clean
	rm -rf $(EXEC)
