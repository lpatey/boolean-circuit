
#include "../../headers/parser/FileInput.h"
#include "../../headers/core/exceptions/TupleDimensionException.h"

#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_directive.hpp>
#include <boost/spirit/include/phoenix_core.hpp>

#include <iostream>
#include <sstream>

typedef std::string::const_iterator StringIterator;

namespace spirit = boost::spirit;
namespace qi = spirit::qi;
namespace phoenix = boost::phoenix;

using std::string;
using std::vector;
using std::map;
using std::ifstream;
using std::ostringstream;

using qi::_1;
using qi::rule;
using qi::phrase_parse;
using qi::char_;
using qi::eoi;
using spirit::lexeme;
using spirit::ascii::no_case;
using spirit::ascii::alpha;
using spirit::ascii::alnum;
using spirit::ascii::space;
using phoenix::push_back;
using phoenix::ref;



FileInput::FileInput (string fileName) : fileName(fileName), inputNames() {
}

FileInput::~FileInput() {
    if( isOpened() ) {
        fileStream.close();
    }
}

void FileInput::open() throw(GateLibException) {
    if( isOpened() ) {
        return;
    }

    fileStream.open( fileName.c_str(), ifstream::in );

    if( !isOpened() ) {
        throw GateLibException ("Cannot open file " + fileName);
    }

    string line;

    rule<StringIterator,std::string()> IDENTIFIER =
        no_case[
            lexeme[
                ((alpha | '_' | '$') >> *(alnum | '_' | '$'))
            ]
        ];



    rule<StringIterator> r = (-IDENTIFIER[ push_back(ref(inputNames), _1) ] % (*space >> ',' >> *space)) >> eoi ;

    getline( fileStream, line );

    StringIterator start = line.begin();
    StringIterator end = line.end();

    if( !phrase_parse (start, end, r, space ) ) {
        throw GateLibException ("Invalid input names");
    }
}

bool FileInput::isOpened() {
    return fileStream.is_open();
}

vector<string>& FileInput::getInputNames() {
    open();
    return inputNames;
}

string FileInput::getFileName() const {
    return fileName;
}

bool FileInput::hasNext() {
    open();
    return fileStream.good();
}

map<string,bool> FileInput::getNext() throw(GateLibException) {
    map<string,bool> inputs;
    vector<string> inputValues;
    string line;

    open();

    rule<StringIterator,std::string()> BOOLEAN = +(char_('0') | char_('1'));
    rule<StringIterator> r = -(BOOLEAN[ push_back(ref(inputValues), _1) ] % (*space >> ',' >> *space)) >> eoi;

    getline( fileStream, line );

    StringIterator start = line.begin();
    StringIterator end = line.end();

    if( !phrase_parse (start, end, r, space) ) {
        throw GateLibException ("Invalid input line");
    }

    if( inputNames.size() != inputValues.size() ) {
        throw TupleDimensionException( "input names", "input values", fileName );
    }

    for( int i=0; i<(int) inputNames.size(); i++ ) {
        string v = inputValues[i];
        if( v.size() == 1 ) {
            inputs[ inputNames[i] ] = v == "1";
            continue;
        }

        for( int j=0; j<(int) v.size(); j++ ) {
            ostringstream oss;
            oss << inputNames[i] << "[" << j << "]";
            inputs[ oss.str() ] = v[j] == '1';
        }
    }

    return inputs;
}
