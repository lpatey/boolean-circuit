
#include "../../../headers/parser/tokens/GateLangTokens.h"
#include <string>
#include <sstream>

using namespace std;

namespace token {

    ParameterToken::ParameterToken() : dim(1) {}

    vector<string> ParameterToken::getNames() {
        vector<string> names;
        if( dim == 1 ) {
            names.push_back(identifier);
        }
        else {
            for (int j = 0 ; j < dim ; j++) {
                ostringstream oss;
                oss << identifier << "[" << j << "]";
                names.push_back(oss.str());
            }
        }
        return names;
    }

    GatePrototypeToken::GatePrototypeToken() : modifier(PUBLIC) {}

    vector<string> GatePrototypeToken::paramsToNames(vector<ParameterToken>& params) {
        vector<string> names;
        int iPSize = params.size();
        for (int i = 0 ; i < iPSize ; i++) {
            ParameterToken& param = params[i];
            vector<string> subNames = param.getNames();
            int subNamesSize = subNames.size();
            for (int j = 0 ; j < subNamesSize ; j++) {
                names.push_back(subNames[j]);
            }
        }

        return names;
    }

    vector<string> GatePrototypeToken::getInputNames() {
        return paramsToNames(inputParams);
    }

    vector<string> GatePrototypeToken::getOutputNames() {
        return paramsToNames(outputParams);
    }

    VariableToken::VariableToken() : type(NORMAL) {}

    string VariableToken::getName() {

        ostringstream oss;
        switch(type) {

            case NORMAL :
                return identifier;
                break;

            case INDEXED :

                oss << identifier << "[" << index << "]";
                return oss.str();
                break;

            case MEMBERED :
                return identifier + "." + member;
                break;
        }

        return "";
    }

    string TemplateIdentifierToken::getName() {
        int paramsSize = params.size();
        if( paramsSize == 0 ) {
            return name;
        }
        ostringstream oss;
        oss << name << "<" << params[0];

        for( int i=1; i<paramsSize; i++ ) {
            oss << "," << params[i];
        }
        oss << ">";
        return oss.str();
    }

}
