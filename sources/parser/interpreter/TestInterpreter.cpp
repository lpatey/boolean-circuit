
#include "../../../headers/parser/interpreter/TestInterpreter.h"
#include "../../../headers/parser/FileContext.h"
#include "../../../headers/core/exceptions/TestFailedException.h"
#include "../../../headers/core/exceptions/TupleDimensionException.h"
#include "../../../headers/core/Clock.h"
#include "../../../headers/core/GateType.h"
#include "../../../headers/core/Gate.h"
#include "../../../headers/Config.h"
#include <string>
#include <vector>
#include <iostream>

using namespace std;
using namespace token;

TestInterpreter::TestInterpreter(TestStatementToken test, FileContext* context) :
    test(test), context(context), index(0) {

    string gateTypeName = test.identifier.getName();
    type = context->getType(gateTypeName);
    if( !type ) {
        throw GateTypeNotFoundException(gateTypeName);
    }

    Gate* gate = Gate::create(type);

    Clock c(gate);
    c.setSpeed(0);
    c.start(*this,*this);

    if( Config::getConfig()->isVerboseEnabled() ) {
        cout << "Test succedeed for " << gateTypeName << endl;
    }
}


void TestInterpreter::setNext(std::map<std::string,bool> values) {
    TestMatchToken& current = test.tests[index-1];
    const vector<string>& outputNames = type->getOutputNames();
    int outputSize = outputNames.size();
    if( outputSize != (int) current.values.size() ) {
        throw TupleDimensionException("match", "outputs", test.identifier.getName() + " test");
    }

    for( int i=0; i<outputSize; i++ ) {
        if( values[ outputNames[i] ] != current.values[i] ) {
            throw TestFailedException(test,values,index-1);
        }
    }
}

bool TestInterpreter::hasNext() {
    return index < (int) test.tests.size();
}

map<string,bool> TestInterpreter::getNext() throw(GateLibException) {
    TestMatchToken& current = test.tests[index++];
    map<string,bool> values;

    const vector<string>& inputNames = type->getInputNames();
    int inputsSize = inputNames.size();

    if( inputsSize != (int) current.matches.size() ) {
        throw TupleDimensionException("match", "inputs", test.identifier.getName() + " test");
    }

    for( int i=0; i<inputsSize; i++ ) {
        values[ inputNames[i] ] = current.matches[i];
    }
    return values;
}
