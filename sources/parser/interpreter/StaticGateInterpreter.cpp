
#include "../../../headers/parser/interpreter/StaticGateInterpreter.h"
#include "../../../headers/core/statics/StaticGateType.h"
#include "../../../headers/core/exceptions/TupleDimensionException.h"
#include "../../../headers/parser/FileContext.h"
#include <boost/variant/get.hpp>
#include <vector>
#include <set>

using namespace std;
using namespace token;

StaticGateInterpreter::StaticGateInterpreter(GateDeclarationToken& declaration, FileContext* context) : declaration(declaration) {

    vector<string> iNames = declaration.prototype.getInputNames();
    vector<string> oNames = declaration.prototype.getOutputNames();
    string gateTypeName = declaration.prototype.identifier;
    vector<InstructionToken>& instructions = declaration.instructions;
    int instructionsSize = instructions.size();

    gateType = new StaticGateType(iNames, oNames, (Context*) context);

    for (int i = 0 ; i < instructionsSize ; i++) {
        boost::apply_visitor(*this, instructions[i]);
    }

    gateType->compute();

    context->setType(gateTypeName, (GateType*) gateType);
}

StaticGateType* StaticGateInterpreter::getGateType() {
    return gateType;
}


void StaticGateInterpreter::operator()(AssignmentToken& token) {
    throw GateLibException ("Assignment in static gate");
}


void StaticGateInterpreter::operator()(ConditionMatchToken& token) {

    vector<MatchResultToken>& results = token.results;
    vector<MatchValueTupleToken>& matches = token.matches;
    int matchesSize = matches.size();
    const vector<string>& inputNames = gateType->getInputNames();

    for( int i=0; i<matchesSize; i++ ) {
        MatchValueTupleToken& match = matches[i];

        // Get all variables
        set<string> variables;
        map<string,bool> variablesExpansion;
        int matchValuesSize = match.values.size();
        for( int j=0; j<matchValuesSize; j++ ) {

            MatchValueToken& mvt = match.values[j];
            if( VariableToken* variable = boost::get<VariableToken>( &mvt ) ) {
                variables.insert( variable->getName() );
            }
            else if( bool* b = boost::get<bool>( &mvt ) ) {
                if( j > (int) inputNames.size() ) {
                    throw TupleDimensionException( "match", "input", declaration.prototype.identifier + " declaration" );
                }
                variablesExpansion[ inputNames[j] ] = *b;
            }
        }

        int resultsSize = results.size();
        for( int i=0; i<resultsSize; i++ ) {
            MatchResultToken& vt = results[i];
            if( VariableToken* variable = boost::get<VariableToken>( &vt ) ) {
                variables.insert( variable->getName() );
            }
        }
        computeEachValueMatching(variables, variablesExpansion, match, results);

    }
}

void StaticGateInterpreter::computeEachValueMatching(set<string> variables,
        map<string,bool> variablesExpansion, MatchValueTupleToken& match,
        vector<MatchResultToken>& results ) {

    if( variables.size() != 0 ) {
        string identifier = *variables.begin();
        variables.erase(identifier);
        variablesExpansion[identifier] = false;
        computeEachValueMatching(variables, variablesExpansion, match, results);
        variablesExpansion[identifier] = true;
        computeEachValueMatching(variables, variablesExpansion, match, results);
        return;
    }

    map<string,bool>::iterator it = variablesExpansion.begin();

    vector<string> inputNames = gateType->getInputNames();

    map<string,bool> expandedValues;
    vector<string> outputNames = gateType->getOutputNames();
    int resultsSize = results.size();
    for( int i=0; i<resultsSize; i++ ) {
        MatchResultToken& vt = results[i];
        if( VariableToken* variable = boost::get<VariableToken>( &vt ) ) {
            expandedValues[ outputNames[i] ] = variablesExpansion[variable->getName()];
        }
        else {
            bool b = boost::get<bool>( vt );
            expandedValues[outputNames[i]] = b;
        }
    }

    gateType->getTruthTable().setValue(variablesExpansion,expandedValues);
}
