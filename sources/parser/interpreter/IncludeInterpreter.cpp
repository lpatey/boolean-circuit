
#include "../../../headers/parser/interpreter/IncludeInterpreter.h"
#include "../../../headers/parser/FileContext.h"
#include "../../../headers/parser/RefactoredContext.h"

using namespace token;
using namespace std;

IncludeInterpreter::IncludeInterpreter(IncludeStatementToken include, FileContext* context) {

    // TODO
    // Modifier PRIVATE/PUBLIC

    vector<FileContext*> subContexts = apply_visitor(*this, include.source);

    int subContextsSize = subContexts.size();
    if( include.refactoringStatements.size() == 0 ) {
        for( int i=0; i<subContextsSize; i++ ) {
            context->addSubContext(subContexts[i]);
        }
    }
    else {
        for( int i=0; i<subContextsSize; i++ ) {
            RefactoredContext* subContext = new RefactoredContext(subContexts[i], include.refactoringStatements);
            context->addSubContext(subContext);
        }
    }
}

vector<FileContext*> IncludeInterpreter::operator() (const RegexToken& regex) {

    // TODO
    return vector<FileContext*>(); // Pour supprimer les warnings en attendant l'implémentation
}

vector<FileContext*> IncludeInterpreter::operator() (const std::string& path) {
    vector<FileContext*> contexts;
    contexts.push_back( FileContext::getContext(path) );
    return contexts;
}

