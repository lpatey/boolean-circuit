
#include "../../../headers/parser/interpreter/RefactoringInterpreter.h"
#include "../../../headers/parser/FileContext.h"
#include <iostream>

using namespace token;
using namespace std;

RefactoringInterpreter::RefactoringInterpreter(RefactoringStatementToken refactoring, FileContext* context) : refactoring(refactoring), context(context) {

    vector<RefactoringPrimitiveToken>& aliases = refactoring.aliases;
    int nAliases = aliases.size();

    if( refactoring.type == MATCH ) {
        types = context->clearTypes();
    }

    for( int i=0; i<nAliases; i++ ) {
        RefactoringPrimitiveToken& primitive = aliases[i];
        variant<vector<string> > destinations = primitive.destinations;
        boost::apply_visitor(*this, primitive.identifier, destinations);
    }
}


void RefactoringInterpreter::operator()(const RegexToken& source, const vector<string>& destinations ) {
    // TODO
}

void RefactoringInterpreter::operator()(const string& source, const vector<string>& destinations ) {

    switch( refactoring.type ) {
        case MATCH :
            for( unsigned int i=0; i<destinations.size(); i++ ) {
                cout << "Will create " << destinations[i] << " with " <<  types[source]  << endl; exit(0);
                context->setType( destinations[i], types[source] );
            }
            if( destinations.size() == 0 ) {
                context->setType( source, types[source] );
            }
            break;

        case ALIAS :
            for( unsigned int i=0; i<destinations.size(); i++ ) {
                context->setType( destinations[i], context->getType(source) );
            }
            break;

        case MOVE :
            for( unsigned int i=0; i<destinations.size(); i++ ) {
                context->setType( destinations[i], context->getType(source) );
            }
            context->unsetType( source );
            break;
    }
}
