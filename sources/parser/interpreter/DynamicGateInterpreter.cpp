#include "../../../headers/parser/interpreter/DynamicGateInterpreter.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/exceptions/GateTypeNotFoundException.h"
#include "../../../headers/core/exceptions/TupleDimensionException.h"
#include "../../../headers/core/statics/StaticGateType.h"
#include "../../../headers/parser/FileContext.h"
#include "../../../headers/core/GateType.h"
#include "../../../headers/Config.h"
#include "../../../headers/core/structures/EdgeStructure.h"
#include "../../../headers/core/structures/GateStructure.h"
#include "../../../headers/parser/interpreter/Variable.h"

#include <boost/variant/get.hpp>
#include <sstream>

using namespace std;
using namespace token;

DynamicGateInterpreter::DynamicGateInterpreter (GateDeclarationToken declaration, FileContext* context)
        : context(context), prototype(declaration.prototype), instructions(declaration.instructions),
        instructionsSize(declaration.instructions.size()) {

    const vector<string>& iNames = prototype.getInputNames();
    const vector<string>& oNames = prototype.getOutputNames();
    string gateTypeName = prototype.identifier;
    gateType = new DynamicGateType(iNames, oNames, (Context*) context);

    declareInputs();
    declareLeftHandSide();
    computeRightHandSide();
    linkOutputs();

    if( gateType->getMemoryBitsNumber() + iNames.size() <= Config::getConfig()->getCacheThreshold() ) {
        gateType->setCacheEnabled(true);
    }

    gateType->compute();
    context->setType(gateTypeName, (GateType*) gateType);

    // Memory free
    map<string,Variable*>::iterator it = declaredVariables.begin();
    for( ; it != declaredVariables.end(); ++it ) {
        delete it->second;
    }
}

void DynamicGateInterpreter::declareInputs() {
    vector<ParameterToken>& inputs = prototype.inputParams;
    int iSize = inputs.size();
    for (int i = 0 ; i < iSize ; i++) {
        ParameterToken& param = inputs[i];
        if( declaredVariables.find(param.identifier) != declaredVariables.end() ) {
            throw GateLibException( "Duplicated input variable " + param.identifier + " in " + prototype.identifier );
        }
        Variable* v = declaredVariables[param.identifier] = new Variable();
        const vector<string>& names = param.getNames();
        int namesSize = names.size();
        for( int j=0; j<namesSize; j++ ) {
            v->add( gateType->getInternalInput(names[j]) );
        }
    }
}

void DynamicGateInterpreter::linkOutputs() {
    string gateTypeName = prototype.identifier;
    vector<ParameterToken>& outputs = prototype.outputParams;
    int oSize = outputs.size();
    for (int i = 0 ; i < oSize ; i++) {
        ParameterToken& param = outputs[i];
        if( declaredVariables.find(param.identifier) == declaredVariables.end() ) {
            throw GateLibException( "Output " + param.identifier + " not set in " + gateTypeName);
        }
        Variable* v = declaredVariables[param.identifier];
        const vector<string>& names = param.getNames();
        unsigned int namesSize = names.size();
        if( namesSize != v->getEdges().size() ) {
            throw TupleDimensionException( "outputs", param.identifier, gateTypeName );
        }
        vector<pair<string,EdgeStructure*> >& edges = v->getEdges();
        for( int i=0; i<namesSize; i++ ) {
            gateType->addInternalOutput( edges[i].second, names[i] );
        }
    }
}

// pseudo done
void DynamicGateInterpreter::declareLeftHandSide() {
    for( int i=0; i<instructionsSize; i++ ) {
        InstructionToken& instruction = instructions[i];

        if( (ConditionMatchToken*) boost::get<ConditionMatchToken>(&instruction) ) {
            throw GateLibException ("Match condition in dynamic gate");
        }
        AssignmentToken token = boost::get<AssignmentToken>(instruction);
        if( GateCallToken* gateCall = boost::get<GateCallToken>( &(token.values) ) ) {
            if( gateCall->valueList.size() > 0 || gateCall->valueMap.size() > 0 || context->hasType( gateCall->identifier ) ) {
                Variable leftHand = computeLeftHandSideOfGateCall( *gateCall );
                computeDeclarations( token.variables, leftHand );
            }
            else {
                GateStructuresMapping mapping;
                gateStructures.push_back( mapping );
            }
            //Variable leftHand = computeLeftHandSideOfGateCall( *gateCall );
            //computeDeclarations( token.variables, leftHand );
        }
        else if( std::vector<VariableToken>* variables = boost::get<std::vector<VariableToken> >( &(token.values) ) ) {
            if( context->hasType( variables->begin()->identifier ) ) {
                Variable leftHand;
                GateType* subType = context->getType(variables->begin()->identifier);
                leftHand.add( subType, gateType );
                computeDeclarations( token.variables, leftHand );
            }
            GateStructuresMapping mapping;
            gateStructures.push_back( mapping );
        }
    }
}


Variable DynamicGateInterpreter::computeLeftHandSideOfGateCall( GateCallToken& gateCall ) {
    Variable v;
    // TODO g�rer les templates
    if (! context->hasType(gateCall.identifier) ) {
        throw GateTypeNotFoundException(gateCall.identifier);
    }

    GateType* type = context->getType(gateCall.identifier);
    GateStructuresMapping mapping = gateType->addGateStructure(type);
    gateStructures.push_back(mapping);

    const vector<string>& outputNames = type->getOutputNames();
    unsigned int outputNamesSize = outputNames.size();
    for( unsigned int i=0; i<outputNamesSize; i++ ) {
        string outputName = outputNames[i];
        v.add( outputName, mapping.outputs[ outputName ]->getOutput( outputName ) );
    }
    return v;

}

void DynamicGateInterpreter::computeRightHandSide() {
    for( int i=0; i<instructionsSize; i++ ) {
        currentIndex = i;
        boost::apply_visitor<DynamicGateInterpreter,InstructionToken>(*this, instructions[i]);
    }
}

bool DynamicGateInterpreter::operator()(ConditionMatchToken& token) {
    throw GateLibException ("Match condition in dynamic gate");
}

bool DynamicGateInterpreter::operator()(AssignmentToken& assignmentToken) {

    TupleExpressionToken& values = assignmentToken.values;

    // Si on fait une simple assignation entre variables
    if (vector<VariableToken>* tokens = boost::get<vector<VariableToken> >(&values)) {

        computeRightHandSideOfTuple(*tokens);
    }
    // Si on assigne le resultat de l'appel d'une porte
    else if( GateCallToken* token = boost::get<GateCallToken>(&values) ) {
        computeRightHandSideOfGateCall(*token);
    }

    return true;
}


void DynamicGateInterpreter::computeDeclarations(std::vector<OptionalVariableToken>& variables, Variable& values) {

    if( variables.size() == 1 ) {
        if( VariableToken* variable = boost::get<VariableToken>( &variables[0] ) ) {
            if( declaredVariables.find(variable->identifier) == declaredVariables.end() ) {
                declaredVariables[variable->identifier] = new Variable();
            }
            Variable* leftVariable = declaredVariables[variable->identifier];
            if( variable->type == NORMAL ) {
                leftVariable->add(values);
            }
            else {
                if( values.getEdges().size() > 1 ) {
                    throw TupleDimensionException( variable->getName(), "right hand side", prototype.identifier );
                }
                leftVariable->set(*variable,values.getEdges().begin()->second);
            }
        }
        return;
    }

    vector<pair<string,EdgeStructure*> >& edges = values.getEdges();
    int edgesSize = values.getEdges().size();
    if( (int) variables.size() != edgesSize ) {
        throw TupleDimensionException( "left hand side", "right hand side", prototype.identifier );
    }
    for( int i=0; i<edgesSize; i++ ) {
        if( VariableToken* variable = boost::get<VariableToken>( &variables[i] ) ) {
            if( declaredVariables.find(variable->identifier) == declaredVariables.end() ) {
                declaredVariables[variable->identifier] = new Variable();
            }
            Variable* leftVariable = declaredVariables[variable->identifier];
            leftVariable->set(*variable, edges[i].second );
        }
    }
}

void DynamicGateInterpreter::computeRightHandSideOfTuple(vector<VariableToken>& variables) {
    Variable v;
    int variablesSize = variables.size();

    if( context->hasType( variables[0].identifier ) ) {
        return;
    }

    for( int i=0; i<variablesSize; i++ ) {
        VariableToken& variable = variables[i];
        if( context->hasType(variable.getName() ) ) {
            GateType* subType = context->getType(variable.getName());
            v.add( subType, gateType );
        }
        else {
            Variable* var = declaredVariables[ variable.identifier ];
            v.add(variable,*var);
        }
    }
    InstructionToken& instruction = instructions[currentIndex];
    AssignmentToken assignment = boost::get<AssignmentToken>(instruction);
    computeDeclarations( assignment.variables, v);
}


void DynamicGateInterpreter::computeRightHandSideOfGateCall(GateCallToken& gateCall) {
    Variable v;
    Variable inputs;
    GateStructuresMapping mapping = gateStructures[currentIndex];
    if( mapping.outputs.size() == 0 ) {
        string name = gateCall.identifier;
        if( declaredVariables.find(name) == declaredVariables.end() ) {
            throw GateLibException( "Undefined variable " + name + " in " + prototype.identifier );
        }

        InstructionToken& instruction = instructions[currentIndex];
        AssignmentToken assignment = boost::get<AssignmentToken>(instruction);
        computeDeclarations( assignment.variables, *declaredVariables[name]);
        return;
    }

    const vector<string>& inputNames = mapping.outputs.begin()->second->getType()->getInputNames();
    int inputNamesSize = inputNames.size();

    // Gestion des variables en arguments
    if( gateCall.type == LIST ) {
        int j=0;

        vector<VariableToken>& variables = gateCall.valueList;
        int variablesSize = variables.size();
        for (int i = 0 ; i < variablesSize ; i++) {
            VariableToken& variable = variables[i];
            Variable edgesVariable;
            if( context->hasType(variable.getName() ) ) {
                GateType* subType = context->getType(variable.getName());
                edgesVariable.add( subType, gateType );
            }
            else {
                Variable* var = declaredVariables[variable.identifier];
                edgesVariable.add(variable,*var);
            }

            vector<pair<string,EdgeStructure*> >& edges = edgesVariable.getEdges();
            int edgesSize = edges.size();
            for( int m=0; m<edgesSize; m++ ) {
                if(inputNamesSize <= j) {
                    throw TupleDimensionException(gateCall.identifier, "inputs", prototype.identifier);
                }
                string inputName = inputNames[j];
                vector<GateStructure*>& subGates = mapping.inputs[inputName];
                int subGatesSize = subGates.size();
                for( int k=0; k<subGatesSize; k++ ) {
                    edges[m].second->addOutput(inputName, subGates[k] );
                }

                j++;
            }
        }
    }
    else {

        vector<LabelledPrimitiveToken>& valueMap = gateCall.valueMap;

        unsigned int j=0;

        unsigned int namesSize = valueMap.size();
        for( unsigned int i=0; i<namesSize; i++ ) {
            LabelledPrimitiveToken& primitive = valueMap[i];
            VariableToken& name = primitive.name;
            VariableToken& value = primitive.value;
            Variable edgesVariable;
            if( context->hasType(value.getName() ) ) {
                GateType* subType = context->getType(value.getName());
                edgesVariable.add( subType, gateType );
            }
            else {
                Variable* var = declaredVariables[value.identifier];
                edgesVariable.add(value,*var);
            }
            vector<pair<string,EdgeStructure*> >& edges = edgesVariable.getEdges();
            int edgesSize = edgesVariable.getEdges().size();
            if( edgesSize > 1 && name.type == NORMAL  ) {
                throw TupleDimensionException(name.getName(),value.getName(), prototype.identifier);
            }

            for( int m=0; m<edgesSize; m++ ) {
                if( inputNames.size() <= j ) {
                    throw TupleDimensionException(gateCall.identifier, "inputs", prototype.identifier);
                }
                string inputName = valueMap[j].name.getName();
                vector<GateStructure*>& subGates = mapping.inputs[inputName];
                unsigned int subGatesSize = subGates.size();
                for( unsigned int k=0; k<subGatesSize; k++ ) {
                    edges[m].second->addOutput( inputName, subGates[k] );
                }

                j++;
            }
        }
    }

}

DynamicGateType* DynamicGateInterpreter::getGateType() {
    return gateType;
}
