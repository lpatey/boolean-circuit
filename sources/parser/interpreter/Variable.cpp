#include "../../../headers/parser/interpreter/Variable.h"
#include "../../../headers/core/GateType.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/exceptions/GateLibException.h"
#include "../../../headers/core/structures/GateStructure.h"
#include <sstream>
#include <iostream>

using namespace std;
using namespace token;

Variable::Variable() {
}

void Variable::add(string name, EdgeStructure* value) {
    pair<string,EdgeStructure*> p(name,value);
    edges.push_back(p);
}

void Variable::add(Variable& v) {
    vector<pair<string,EdgeStructure*> >& subEdges = v.edges;
    for( unsigned int i=0; i<subEdges.size(); i++ ) {
        add( subEdges[i].first, subEdges[i].second );
    }
}

void Variable::add(GateType* type, DynamicGateType* parent) {
    GateStructuresMapping mapping = parent->addGateStructure(type);
    map<string,GateStructure*>::iterator it = mapping.outputs.begin();;
    for( ; it != mapping.outputs.end(); ++it ) {
        add( it->first, it->second->getOutput(it->first) );
    }
}

void Variable::add(VariableToken& token, Variable& variable) {
    if( token.type == NORMAL ) {
        add(variable);
        return;
    }
    EdgeStructure* structure = variable.get(token);
    if( token.type == MEMBERED ) {
        add(token.member, structure);
        return;
    }
    add(structure);
}

void Variable::add(EdgeStructure* structure) {
    ostringstream oss;
    oss << edges.size();
    edges.push_back( pair<string,EdgeStructure*>( oss.str(), structure ) ) ;
}

vector<pair<string, EdgeStructure*> >& Variable::getEdges() {
    return edges;
}

void Variable::set(VariableToken& variable, EdgeStructure* structure) {
    if( variable.type == INDEXED ) {
        if( variable.index >= (int) edges.size() ) {
            throw GateLibException( "Index out of bound for variable " + variable.identifier );
        }
        edges[variable.index].second = structure;
    }
    else {

        string name = variable.type == MEMBERED ? variable.member : variable.identifier;
        for( unsigned int i=0; i<edges.size(); i++ ) {
            if( edges[i].first == name ) {
                edges[i].second = structure;
                return;
            }
        }
        return add(name,structure);
    }
}

EdgeStructure* Variable::get(VariableToken& variable) {
    if( variable.type == INDEXED ) {
        if( variable.index >= (int) edges.size() ) {
            throw GateLibException( "Index out of bound for variable " + variable.identifier );
        }
        return edges[variable.index].second;
    }
    else if( variable.type == MEMBERED ) {
        return get(variable.member);
    }
    else {
        return get(variable.identifier);
    }
}

EdgeStructure* Variable::get(string name) {
    for( unsigned int i=0; i<edges.size(); i++ ) {
        if( edges[i].first == name ) {
            return edges[i].second;
        }
    }
    throw GateLibException( "Unknown variable " + name );
}

