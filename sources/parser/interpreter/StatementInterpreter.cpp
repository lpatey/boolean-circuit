#include "../../../headers/parser/interpreter/StatementInterpreter.h"
#include "../../../headers/parser/interpreter/StaticGateInterpreter.h"
#include "../../../headers/parser/interpreter/DynamicGateInterpreter.h"
#include "../../../headers/parser/interpreter/IncludeInterpreter.h"
#include "../../../headers/parser/interpreter/RefactoringInterpreter.h"
#include "../../../headers/parser/interpreter/TestInterpreter.h"
#include "../../../headers/parser/FileContext.h"
#include "../../../headers/core/GateType.h"
#include "../../../headers/core/exceptions/GateLibException.h"
#include <boost/variant/get.hpp>

#include <vector>
#include <string>
#include <iostream>

using namespace std;
using namespace token;

StatementInterpreter::StatementInterpreter(FileContext* context) : context(context) {
}

void StatementInterpreter::operator()(GateDeclarationToken& gateDeclaration) {
    vector<InstructionToken>& instructions = gateDeclaration.instructions;
    int instructionsSize = instructions.size();
    string gateTypeName = gateDeclaration.prototype.identifier;

    if (instructionsSize == 0) {
        throw GateLibException("porte " + gateTypeName + " est vide !");
    }

    if( (AssignmentToken*) boost::get<AssignmentToken>( &instructions[0]) ) {
        DynamicGateInterpreter(gateDeclaration, context);
    }
    else {
        StaticGateInterpreter(gateDeclaration, context);
    }
}

void StatementInterpreter::operator()(IncludeStatementToken& statement) {

    IncludeInterpreter(statement, context);
}

void StatementInterpreter::operator()(RefactoringStatementToken& statement) {

    RefactoringInterpreter(statement, context);
}

void StatementInterpreter::operator()(TestStatementToken& statement) {

    TestInterpreter(statement, context);
}
