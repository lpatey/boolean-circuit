
#include "../../headers/parser/RefactoredContext.h"
#include "../../headers/parser/FileContext.h"

using namespace std;
using namespace token;

RefactoredContext::RefactoredContext(FileContext* context, vector<RefactoringStatementToken> refactorings)
    : context(context), refactorings(refactorings) {

    set<string> names = context->getTypeNames();
    set<string>::iterator it = names.begin();
    for( ; it != names.end(); ++it ) {
        aliases[*it] = *it;
    }

    int refactoringsSize = refactorings.size();
    for( int i=0; i<refactoringsSize; i++ ) {
        applyRefactoring(refactorings[i]);
    }

}

bool RefactoredContext::hasType(string name) {
    if( aliases.find(name) == aliases.end() ) {
        return false;
    }
    return context->hasType(aliases[name]);
}

GateType* RefactoredContext::getType(std::string name) throw(GateTypeNotFoundException) {
    if( aliases.find(name) == aliases.end() ) {
        throw GateTypeNotFoundException(name);
    }
    return context->getType(aliases[name]);
}

set<string> RefactoredContext::getTypeNames() {
    set<string> names;
    map<string,string>::iterator it = aliases.begin();
    for( ; it != aliases.end(); ++it ) {
        names.insert( it->first );
    }
    return names;
}

void RefactoredContext::applyRefactoring(RefactoringStatementToken& refactoring) {
    int nAliases = refactoring.aliases.size();
    map<string,string> types;
    currentRefactoring = refactoring;

    if( refactoring.type == MATCH ) {
        map<string,string>::iterator it = aliases.begin();
        for( ; it != aliases.end(); ++it ) {
            types[ it->first ] = it->second;
        }
        aliases.clear();
    }

    for( int i=0; i<nAliases; i++ ) {
        RefactoringPrimitiveToken& primitive = refactoring.aliases[i];
        variant<vector<string> > destinations = primitive.destinations;
        boost::apply_visitor(*this, primitive.identifier, destinations);
    }
}

void RefactoredContext::operator()(const RegexToken& source, const vector<string>& destinations ) {
    // TODO
}

void RefactoredContext::operator()(const string& source, const vector<string>& destinations ) {
    int destinationsSize = destinations.size();
    switch( currentRefactoring.type ) {
        case MATCH :
            for(int i=0; i < destinationsSize; i++) {
                aliases[destinations[i]] = source;
            }
            if( destinations.size() == 0 ) {
                aliases[source] = source;
            }
            break;

        case ALIAS :
            for(int i=0; i < destinationsSize; i++) {
                aliases[destinations[i]] = source;
            }
            break;

        case MOVE :
            for(int i=0; i < destinationsSize; i++) {
                aliases[destinations[i]] = source;
            }
            aliases.erase(source);
            break;
    }
}

