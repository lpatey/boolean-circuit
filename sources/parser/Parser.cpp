
#include "../../headers/parser/Parser.h"
#include "../../headers/Config.h"
#include "../../headers/core/exceptions/GateLibException.h"

#include <boost/spirit/include/qi.hpp>

#include <iostream>
#include <fstream>
#include <vector>

using namespace std;
using boost::spirit::qi::grammar;
using boost::spirit::qi::phrase_parse;
using boost::spirit::ascii::space;

Parser::Parser(GateLangGrammar& g) : myGrammar(g) {
}

bool Parser::parse(string filename, std::vector<token::StatementToken>& statements)
{
    ifstream in(filename.c_str(), ios_base::in);

    if( Config::getConfig()->isVerboseEnabled() ) {
        cout << "Opening " << filename << "... " << (in.is_open () ? "done" : "failed" ) << endl;
    }

    if ((! in.is_open()) || in.fail())
    {
        throw GateLibException( "Could not open input file: " + filename );
        return false;
    }

    in.unsetf(ios::skipws); //  Turn off white space skipping on the stream

    string storage;
    std::copy(
        istream_iterator<char>(in),
        istream_iterator<char>(),
        std::back_inserter(storage));
    in.close();

    StringIterator start = storage.begin();
    StringIterator end = storage.end();

    SkipGrammar skip;

   return phrase_parse(start, end, myGrammar.context, skip, statements);
}
