
#include "../../headers/parser/FileContext.h"
#include "../../headers/parser/grammars/GateLangGrammar.h"
#include "../../headers/parser/Parser.h"
#include "../../headers/parser/interpreter/StatementInterpreter.h"

#include <boost/filesystem/path.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <string>
#include <utility>

using namespace std;
using namespace token;
namespace fs = boost::filesystem;

map<string,FileContext*> FileContext::contexts;

FileContext::FileContext (string path) throw(GateLibException) : DefaultContext(), 
        subContexts(), subContextsSize(0) {

    GateLangGrammar g;
    Parser p(g);

     if( !p.parse( path, statements ) ) {
        throw GateLibException( path + " fails parsing" );
    }
}

FileContext* FileContext::getContext(string path) throw(GateLibException) {
    fs::path p(path);
    path = p.normalize().string();
    map<string,FileContext*>::iterator it = contexts.find(path);

    if( it == contexts.end() ) {
        FileContext* f = new FileContext (path);
        f->compute();
        contexts[path] = f;
    }
    return contexts[path];
}

void FileContext::addSubContext (Context* context) {
    subContexts.push_back(context);
    subContextsSize++;
}

void FileContext::compute () {
    StatementInterpreter interpreter(this);
    int nbrStatements = statements.size();
    for (int i = 0 ; i < nbrStatements ; i++) {
        StatementToken statement = statements[i];
        boost::apply_visitor(interpreter, statement);
    }
}

void FileContext::unsetType(string name) {
    types.erase(name);
}

map<string,GateType*> FileContext::clearTypes() {
    map<string,GateType*> previousTypes = types;
    types.clear();
    return previousTypes;
}
/*
DynamicGateType* FileContext::createDynamicGateType(vector<string> iNames, vector<string> oNames, vector<InstructionToken> instructions) {
    DynamicGateType* type = new DynamicGateType (iNames, oNames, (Context*) this);
    map<string, Variable*> variables;
    int iSize = iNames.size(), oSize = oNames.size();
    for (int i = 0 ; i < iSize ; i++) {
        makeVariableInput(variables, iNames[i]);
    }
    for (int i = 0 ; i < oSize ; i++) {
        makeVariableOutput(variables, oNames[i]);
    }

    int nbrInstructions = instructions.size();
    for (int i = 0 ; i < nbrInstructions ; i++) {
        AssignmentToken* assignmentToken = instructions[i].assignment;
        if (assignmentToken == 0) {
            throw GateLibException ("porte ni statique ni dynamique");
        }

        computeAssignmentInstruction (assignmentToken, type, variables);
    }

    type->compute();
}

void FileContext::computeAssignmentInstruction (AssignmentToken* assignmentToken,
    DynamicGateType* type, map<string, Variable*>& variables_map) {

        vector<OptionalVariableToken> variables = assignmentToken->variables;
        TupleExpressionToken values = assignmentToken->values;

        switch(values.type) {
                case GATE_CALL :
                    computeGateCall(values->gateCall, type, variables_map, variables);
                    break;

                case VALUE_LIST :
                    computeEqualValues(values->values, type, variables_map, variables);
                    break;
        }
}

void FileContext::computeGateCall(GateCallToken* gateCall, DynamicGateType* type,
        map<string, Variable*>& variables_map, vector<OptionalVariableToken> variables) {

    if (gateCall == 0) {
        throw GateLibException("Porte d'identifieur non d�fini dans l'arbre syntaxique");
    }

     // TODO
}

void FileContext::computeEqualValues(vector<ValueToken> values, DynamicGateType* type,
        map<string, Variable*>& variables_map, vector<OptionalVariableToken> variables) {

    int nbrVariables = variables.size(), nbrValues = values.size();
    if (nbrVariables == 0 || nbrValues == 0) {
        throw GateLibException ("Pas le m�me nombre de variables de part et d'autre de l'assignation");
    }

     // TODO
}

StaticGateType* FileContext::createStaticGateType (vector<string> iNames, vector<string> oNames, vector<InstructionToken> instructions) {
    StaticGateType* type = new StaticGateType(iNames, oNames, (Context*) this);
    BinaryTree& truthTable = type->getTruthTable();

    int nbrInstructions = instructions.size();
    for (int i = 0 ; i < nbrInstructions ; i++) {
        ConditionMatchToken* matchToken = instructions[i].match;
        if (matchToken == 0) {
            throw GateLibException ("porte ni statique ni dynamique");
        }

        computeMatchingInstruction (matchToken, type);
    }

    type->compute();
}
*/

bool FileContext::hasType(string name) {
    map<string,GateType*>::iterator it = types.find(name);
    if( it != types.end() ) {
        return true;
    }

    for(int i = 0 ; i < subContextsSize ; i++) {
        Context* subContext = subContexts[i];
        if( subContext->hasType(name) ) {
            return true;
        }
    }
    return false;
}


GateType* FileContext::getType(string name) throw(GateTypeNotFoundException) {
    map<string,GateType*>::iterator it = types.find(name);
    if( it != types.end() ) {
        return types[name];
    }

    for( int i = 0; i < subContextsSize ; i++ ) {
        Context* subContext = subContexts[i];
        if( subContext->hasType(name) ) {
            return subContext->getType(name);
        }
    }

    throw GateTypeNotFoundException(name);
}


set<string> FileContext::getTypeNames() {
    set<string> names;
    for(int i = 0 ; i < subContextsSize ; i++) {
        set<string> subNames = subContexts[i]->getTypeNames();
        set<string>::iterator subIt = subNames.begin();
        for( ; subIt != subNames.end(); ++subIt ) {
            names.insert( *subIt );
        }
    }
    map<string,GateType*>::iterator it = types.begin();
    for( ; it != types.end(); ++it ) {
        names.insert( it->first );
    }
    return names;
}
