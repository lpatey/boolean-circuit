
#include "../../../headers/parser/grammars/Primitives.h"

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/phoenix/fusion/at.hpp>
#include <boost/spirit/home/phoenix/container.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_directive.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <utility>

using namespace token;
namespace px = boost::phoenix;


using spirit::ascii::alpha;
using spirit::ascii::alnum;
using spirit::ascii::graph;
using spirit::ascii::space;
using spirit::ascii::print;
using spirit::ascii::no_case;
using qi::lexeme;
using qi::lit;
using qi::char_;
using qi::int_;
using qi::symbols;
using qi::phrase_parse;
using qi::eoi;
using qi::on_error;
using qi::fail;
using qi::_val;
using qi::_1;
using qi::_2;
using qi::_3;
using qi::_4;
using qi::omit;
using px::construct;
using px::new_;
using px::val;
using px::push_back;
using px::at_c;
using px::ref;


Primitives::Primitives() {

    // operators
    ASSIGN_OP = char_('=');
    RIGHT_ARROW_OP = lit("->");
    MEMBER_OP = char_('.');

    // delimiters
    LEFT_BRACE = char_('{');
    RIGHT_BRACE = char_('}');
    LEFT_BRACKET = char_('[');
    RIGHT_BRACKET = char_(']');
    START_TEMPLATE = char_('<');
    END_TEMPLATE = char_('>');
    LEFT_PAREN = char_('(');
    RIGHT_PAREN = char_(')');
    LIST_SEP = char_(',');

    // tokens
    GATE_TK = lit("gate");
    INCLUDE_TK = lit("include");
    AS_TK = lit("as");
    PRIVATE_TK = lit("private");
    PUBLIC_TK = lit("public");
    ALIAS_TK = lit("alias");
    MATCH_TK = lit("match");
    MOVE_TK = lit("move");
    TEST_TK = lit("test");
    SELF_TK = lit("self");

    // keywords
    keywords =
        lit("gate")
        | lit("include")
        | lit("as")
        | lit("private")
        | lit("public")
        | lit("alias")
        | lit("match")
        | lit("move")
        | lit("test")
        | lit("self")
        | lit("_");

    STATEMENT_SEP = lexeme[ char_(';') | '\n' | '\r' ];

    IDENTIFIER =
        no_case[
            lexeme[
                (alpha | '_' | '$') >> *(alnum | '_' | '$')
            ] - keywords
        ];

    gateIdentifier =
        (char_('A','Z')[_val += _1] | (graph[_val += _1] - alpha)) >>
        no_case[
            lexeme[
                *(graph[_val += _1] - (char_(',') | '<' | '>' | '[' | ']' | '.' | ';' | '{' | '}' | char_(':') ) )
            ] - keywords
        ];

    QUOTED_STRING =
        lexeme[
            omit[char_('"')] >>
            *( ( char_('\\') >> char_[_val += _1] ) | (char_[_val += _1]-'"') ) >>
            omit[char_('"')]
        ];

    STRING %= QUOTED_STRING | IDENTIFIER;

    INTEGER %= int_;

    INDEX %= INTEGER;

    BOOLEAN =
        no_case[
            char_('0')[_val = false] | char_('1')[_val = true]
        ];

    VARIABLE =
        IDENTIFIER[at_c<1>(_val) =_1] >>
        -(
            (LEFT_BRACKET >> INDEX[at_c<0>(_val) = token::INDEXED][at_c<3>(_val) =_1] >> RIGHT_BRACKET)
            |(MEMBER_OP >> IDENTIFIER[at_c<0>(_val) = token::MEMBERED][at_c<2>(_val) =_1])
        );

    MODIFIER =
        no_case[
            PRIVATE_TK[_val=token::PRIVATE] | PUBLIC_TK[_val=token::PUBLIC]
        ];

    GATE_KEYWORD =
        no_case[
            GATE_TK
        ];

    INCLUDE_KEYWORD =
        no_case[
            INCLUDE_TK
        ];

    ALIAS_KEYWORD =
        no_case[
            ALIAS_TK[_val = token::ALIAS]
        ];

    AS_KEYWORD =
        no_case[
            AS_TK
        ];

    TEST_KEYWORD =
        no_case[
            TEST_TK
        ];

    SELF_KEYWORD =
        no_case[
            SELF_TK
        ];

    MATCH_KEYWORD =
        no_case[
            MATCH_TK[_val = token::MATCH]
        ];

    MOVE_KEYWORD =
        no_case[
            MOVE_TK[_val = token::MOVE]
        ];

    REGEX_BODY = lexeme[
           *((char_('\\') >> char_[_val += _1]) |(graph[_val += _1] - '/'))
        ];

    REGEX_OPTIONS = lexeme[
            *(alpha[_val += _1] | char_('_')[_val += _1])
        ];

    regex %=
        '/' >> REGEX_BODY >>
            '/' >> REGEX_OPTIONS
        ;

    regexOrString %= regex | STRING;

    DESTEX %= gateIdentifier;

    TYPE_DECLARATION = (char_(':') >> LEFT_BRACKET >> INTEGER[_val = _1] >> RIGHT_BRACKET);

    variableDeclaration = IDENTIFIER[at_c<0>(_val) = _1] >> -(TYPE_DECLARATION[at_c<1>(_val) = _1]);

        // List
    paramsList =
        (
            LEFT_PAREN >>
                (variableDeclaration[push_back(_val,_1)] % LIST_SEP) >>
            RIGHT_PAREN
        ) |
        (
            variableDeclaration[push_back(_val,_1)] % LIST_SEP
        );
}
