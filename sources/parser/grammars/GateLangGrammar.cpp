
#include "../../../headers/parser/grammars/GateLangGrammar.h"

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/home/phoenix/fusion/at.hpp>
#include <boost/spirit/home/phoenix/container.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/qi_directive.hpp>
#include <boost/spirit/include/qi_omit.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_object.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <utility>

using namespace token;
namespace px = boost::phoenix;


using spirit::ascii::alpha;
using spirit::ascii::alnum;
using spirit::ascii::graph;
using spirit::ascii::space;
using spirit::ascii::print;
using spirit::ascii::no_case;
using qi::lexeme;
using qi::lit;
using qi::char_;
using qi::int_;
using qi::symbols;
using qi::phrase_parse;
using qi::eoi;
using qi::on_error;
using qi::fail;
using qi::_val;
using qi::_1;
using qi::_2;
using qi::_3;
using qi::_4;
using qi::omit;
using px::construct;
using px::new_;
using px::val;
using px::push_back;
using px::at_c;
using px::ref;

GateLangGrammar::GateLangGrammar() : GateLangGrammar::base_type( context, "GateLang" ) {

    // Template
    templateParameter %= p.INTEGER | p.variableDeclaration | char_('_');

    templateTuple = (
            p.LEFT_PAREN >>
                (templateParameter[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            templateParameter[push_back(_val,_1)] % p.LIST_SEP
        );

    templateValueTuple = (
            p.LEFT_PAREN >>
                (p.INTEGER[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            p.INTEGER[push_back(_val,_1)] % p.LIST_SEP
        );

    templateIdentifier = p.gateIdentifier[at_c<0>(_val) = _1] >>
        -(p.START_TEMPLATE >> (
            templateValueTuple[at_c<1>(_val) = _1]
        ) >> p.END_TEMPLATE);



    // Match
    conditionMatch =
        +('|' >> -matchValueTuple[push_back(at_c<0>(_val), _1)]) >> p.RIGHT_ARROW_OP >> -matchResultTuple[at_c<1>(_val) = _1];

    matchValue %= p.VARIABLE | p.BOOLEAN | char_('_');

    matchResult %= p.VARIABLE | p.BOOLEAN;

    matchResultTuple =
        (
            p.LEFT_PAREN >>
                (matchResult[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            matchResult[push_back(_val,_1)] % p.LIST_SEP
        );

    matchValueTuple =
        (
            p.LEFT_PAREN >>
                (matchValue[push_back(at_c<0>(_val),_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            matchValue[push_back(at_c<0>(_val),_1)] % p.LIST_SEP
        );

    booleanTuple =
        (
            p.LEFT_PAREN >>
                (p.BOOLEAN[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            p.BOOLEAN[push_back(_val,_1)] % p.LIST_SEP
        );

    labelledPrimitive = (char_(':')|char_('.')) >> p.VARIABLE[at_c<0>(_val) = _1] >>
        p.ASSIGN_OP >> p.VARIABLE[at_c<1>(_val) = _1];

    labelledTuple =
        (
            p.LEFT_PAREN >>
                (labelledPrimitive[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            labelledPrimitive[push_back(_val,_1)] % p.LIST_SEP
        );

    variableTuple =
        (
            p.LEFT_PAREN >>
                (p.VARIABLE[push_back(_val,_1)] % p.LIST_SEP) >>
            p.RIGHT_PAREN
        ) |
        (
            p.VARIABLE[push_back(_val,_1)] % p.LIST_SEP
        );

    optionalVariable %= p.VARIABLE | char_('_');

    optionalVariableTuple =
        (
            p.LEFT_PAREN >>
                (optionalVariable[push_back(_val,_1)]) % p.LIST_SEP >>
            p.RIGHT_PAREN
        ) |
        (
            optionalVariable[push_back(_val,_1)] % p.LIST_SEP
        );

    tupleExpression %= gateCall | variableTuple;

    assignmentExpression %= (optionalVariableTuple >> p.ASSIGN_OP >> tupleExpression);

    // Gate
    gateCall =
        p.gateIdentifier[at_c<0>(_val) = _1] >>
        -(p.START_TEMPLATE >> templateValueTuple[at_c<1>(_val) = _1] >> p.END_TEMPLATE ) >>
        -(
            (variableTuple[at_c<2>(_val) = token::LIST][at_c<3>(_val) = _1])
            | (labelledTuple[at_c<2>(_val) = token::MAP][at_c<4>(_val) = _1])
        );

    gatePrototype =
        (-p.MODIFIER[at_c<0>(_val) = _1] >>
        -p.GATE_KEYWORD >> -p.paramsList[at_c<3>(_val) = _1] >>
        (-char_('=')) >>
        p.gateIdentifier[at_c<1>(_val) = _1] >>
        -(p.START_TEMPLATE >> templateTuple[at_c<4>(_val) = _1] >> p.END_TEMPLATE ) >>
        -p.paramsList[at_c<2>(_val) = _1]);

    instruction %= assignmentExpression | conditionMatch;

    gateDeclaration =
        gatePrototype[at_c<0>(_val) = _1] >> p.LEFT_BRACE >>
            (instruction[push_back(at_c<1>(_val),_1)] % p.STATEMENT_SEP)
            >> -p.STATEMENT_SEP >>
        p.RIGHT_BRACE;

    // Include
    includeStatement = -p.MODIFIER[at_c<0>(_val) = _1] >> p.INCLUDE_KEYWORD >>
        p.regexOrString[at_c<1>(_val) = _1] >>
        *refactoringGroup[push_back(at_c<2>(_val),_1)];

    // Tests
    testMatch %= '&' >> -booleanTuple >> p.RIGHT_ARROW_OP >> -booleanTuple;

    testStatement = p.TEST_KEYWORD >> templateIdentifier[at_c<0>(_val)=_1] >> p.LEFT_BRACE >>
        (testMatch[push_back(at_c<1>(_val),_1)] % p.STATEMENT_SEP) >> -p.STATEMENT_SEP
        >> p.RIGHT_BRACE;

    // Refactoring
    refactoringPrimitive = p.regexOrString[at_c<0>(_val) = _1] >> -(p.AS_KEYWORD >> (p.DESTEX[push_back(at_c<1>(_val),_1)] % p.LIST_SEP) );

    refactoringList =
        p.LEFT_BRACE >>
            *(refactoringPrimitive[push_back(_val,_1)] % p.STATEMENT_SEP) >> -p.STATEMENT_SEP >>
        p.RIGHT_BRACE;

    refactoringGroup %= (p.ALIAS_KEYWORD|p.MATCH_KEYWORD|p.MOVE_KEYWORD) >> refactoringList;

    refactoringStatement %= p.SELF_KEYWORD >> refactoringGroup;

    statement %= gateDeclaration
        | refactoringStatement
        | includeStatement
        | testStatement;

    // Entry
    context =
        *statement[push_back(_val,_1)] >> eoi;

    on_error<fail> (
        context
      , std::cout
            << val("Error! Expecting ")
            << _4                               // what failed?
            << val(" here: \"")
            << construct<std::string>(_3, _2)   // iterators to error-pos, end
            << val("\"")
            << std::endl
    );

}

bool GateLangGrammar::parseTemplate(std::string name, TemplateIdentifierToken& identifier) {

    StringIterator start = name.begin();
    StringIterator end = name.end();
    CommentsSkipper skip;

    return phrase_parse (start, end, templateIdentifier, skip, identifier);
}
