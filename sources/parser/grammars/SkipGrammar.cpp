
#include "../../../headers/parser/grammars/SkipGrammar.h"

#include <boost/spirit/home/qi/char.hpp>
#include <boost/spirit/home/qi/operator.hpp>
#include <boost/spirit/home/qi/string.hpp>

using boost::spirit::ascii::space;
using boost::spirit::qi::char_;

SkipGrammar::SkipGrammar() : SkipGrammar::base_type( skip )
{

    skip
        =   space
        |   "//" >> *(char_ - char_('\n')) >> '\n'     // C++ comment
        |   "/*" >> *(char_ - "*/") >> "*/"     // C comment
        |   "#" >> *(char_ - '\n') >> '\n'      // Shell comment
        |   "%" >> *(char_ - '\n') >> '\n'      // I don't know comment
        |   "(*" >> *(char_ - "*)") >> "*)"     // ML comment

        ;
}

