
#include "../../headers/parser/StandardOutput.h"
#include <iostream>

using namespace std;


void StandardOutput::setNext(map<string,bool> values) {
    map<string,bool>::iterator
        it = values.begin(),
        itEnd = values.end();

    for( ; it != itEnd; ++it ) {
        cout << it->first << "=" << it->second << " ";
    }

    cout << endl;
}
