
#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>

#include "../headers/Config.h"

using namespace std;
namespace po = boost::program_options;

Config* Config::config = 0;

Config::Config(int argc, char* argv[]) : visible ("Available options") {

    po::options_description generic;
    generic.add_options()
        ("help,h", "Produce help message")
        ("version,v", "Display the version of GateLib used")
        ("verbose,p", "Enable verbose mode")
        ;

    po::options_description config;
    config.add_options()
        /*("include-paths,I", po::value<string> () ->composing(), "paths of the .gl files to include")*/
        ("cache-threshold,c", po::value<int> () -> default_value(5), "Threshold on the number of entries under which truthtables will be stored")
        ("clock-speed,s", po::value<int> () -> default_value(0), "Number of nanoseconds between each clock tick. Set to 0 to get the best possible speed")
        ;

    po::options_description hidden; // no more hidden in truth ...
    hidden.add_options()
        ("file", po::value<string> (), "Path to the file describing the circuit")
        ("gate", po::value<string> (), "Name of the gate to simulate")
        ("input-file", po::value<string>(), "Path to the input file")
        ;

    cmdLineOptions.add(generic).add(config).add(hidden);
    configFileOptions.add(config).add(hidden);
    visible.add(generic).add(config).add(hidden);

    po::positional_options_description p;
    p.add("file", 1);
    p.add("gate", 1);
    p.add("input-file", 1);

    ifstream ifs ("GateLibConfig.cfg");
    po::store(po::command_line_parser(argc, argv).
              options(cmdLineOptions).positional(p).run(), vm);
    po::store(po::parse_config_file(ifs , configFileOptions), vm);
    po::notify(vm); // Inutile actuellement, mais ne co�te rien et peut �viter certains bugs � l'avenir.
}

Config* Config::getConfig() {
    return config;
}

Config* Config::init (int argc, char* argv[]) {
   return config = new Config(argc,argv);
}



bool Config::compute () {
    bool isComplete = vm.count ("file");
    if (vm.count ("help") || ((! isComplete) && !vm.count ("version")) ) {

        cout << endl;
        cout << "Syntax :" << endl;
        cout << endl;
        cout << "   GateLib netlist.gl [options]" << endl;
        cout << "   GateLib netlist.gl gateName input.gli [options]" << endl;
        cout << endl;

        cout << visible << "\n";
        isComplete = false;
    }

    if (vm.count ("version")) {
        cout << VERSION << "\n";
        isComplete = false;
    }

    return isComplete;
}

string Config::getFilePath () {
    return (vm["file"].as<string>());
}

string Config::getSimulatedGate () {
    return (vm["gate"].as<string>());
}

string Config::getInputFilePath () {
    return (vm["input-file"].as<string>());
}
vector<string> Config::getIncludePaths () {
    return (vm["include_paths"].as<vector<string> >());
}

int Config::getCacheThreshold () {
    return (vm["cache-threshold"].as<int>());
}

int Config::getClockSpeed () {
    return (vm["clock-speed"].as<int>());
}

bool Config::isVerboseEnabled() {
    return vm.count("verbose");
}

bool Config::isComplete () {
    return vm.count("gate") && vm.count("input-file");
}
