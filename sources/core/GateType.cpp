#include "../../headers/core/GateType.h"

using namespace std;

GateType::GateType (vector<string> inputNames, vector<string> outputNames, Context* context ) :
        inputNames(inputNames), outputNames(outputNames), context(context), finished(false) {
}

GateType::~GateType() {
}

std::vector<std::string> GateType::getInputNames () const {
    return inputNames;
}

std::vector<std::string> GateType::getOutputNames () const {
    return outputNames;
}

Context* GateType::getContext() const {
    return context;
}

void GateType::compute () {
    finished = true;
}

bool GateType::isFinished () {
    return finished;
}
