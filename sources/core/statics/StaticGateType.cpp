#include "../../../headers/core/statics/StaticGateType.h"
#include "../../../headers/core/algorithms/ReductionMaker.h"
#include "../../../headers/core/algorithms/StaticConnectionMatrixBuilder.h"

using namespace std;

StaticGateType::StaticGateType(vector<string> inputNames, vector<string> outputNames, Context* context)
    : GateType(inputNames, outputNames, context), truthTable(), gateContainer(),
    connectionMatrix() {

    int iSize = inputNames.size(), oSize = outputNames.size();
    for (int i = 0 ; i < iSize ; i++) {
        for (int j = 0 ; j < oSize ; j++) {
            connectionMatrix[pair<string, string> (inputNames[i], outputNames[j])]
                    = NONE;
        }
    }

}

MetaType StaticGateType::getMetaType() {
	return STATIC;
}

BinaryTree& StaticGateType::getTruthTable() {
	return truthTable;
}

void StaticGateType::setTruthTable (BinaryTree tree) {
	truthTable = tree;
}

int StaticGateType::getMemoryBitsNumber() {
    return 0;
}

ConnectionType StaticGateType::connectionType(string inputName, string outputName) {
	return connectionMatrix[pair<string, string> (inputName, outputName)];
}

GateContainer* StaticGateType::getGateContainer() {
	return &gateContainer;
}

void StaticGateType::compute() throw (GateLibException){
    GateType::compute();
    StaticConnectionMatrixBuilder scmb (this);
    scmb.compute ();
	ReductionMaker maker (this);
	maker.compute();
}

void StaticGateType::declareConnected (string inputName, string outputName) {
    connectionMatrix[pair<string, string> (inputName, outputName)] = COMBINATORY;
}
