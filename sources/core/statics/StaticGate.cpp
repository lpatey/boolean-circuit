
#include "../../../headers/core/statics/StaticGate.h"
#include "../../../headers/core/Edge.h"
#include "../../../headers/core/statics/StaticGateType.h"
#include <iostream>

using namespace std;

StaticGate::StaticGate(StaticGateType* argType) : Gate((GateType*) argType), type(argType), memBits() {
}

GateType* StaticGate::getType() const {
	return (GateType*) type;
}

StaticGateType* StaticGate::getTrueType () const {
	return type;
}

map<string, MemoryBit*> StaticGate::getMemoryBits() {
	return memBits;
}

void StaticGate::tick(bool b) {
    //cout << endl;
    //cout << "Appel de tick de " << this << endl;
	if (!hasSignal()) {
	} else {
		clearSignal();
		map<string, bool> entries =  map<string, bool>();
		{
		    map<string, Edge*> inputEdges = getInputEdges();
            map<string, Edge*>::iterator
                itBegin = inputEdges.begin(),
                itEnd = inputEdges.end();
            for (; itBegin != itEnd ; ++itBegin) {
                entries[itBegin->first] = itBegin->second->getValue();
            }
		}
		map<string, bool> result = getTrueType()->getTruthTable().getValue(entries);
        {
            map<string, Edge*> outputEdges = getOutputEdges();
            map<string, Edge*>::iterator
                itBegin = outputEdges.begin(),
                itEnd = outputEdges.end();
            for (; itBegin != itEnd ; ++itBegin) {
               // cout << "Set value " << itBegin->first << " = " << result[itBegin->first] << endl;
                itBegin->second->setValue(result[itBegin->first]);
            }
        }
	}
}
