#include "../../headers/core/Edge.h"
#include "../../headers/core/Gate.h"

using namespace std;

Edge::Edge () : value(false), outputs() {
}

void Edge::addOutput (string name, Gate* gate) {
    outputs.push_back(pair<string,Gate*>(name, gate));
    gate->setInputEdge(name, this);
}

vector<pair<string,Gate*> >& Edge::getOutputs () {
    return outputs;
}


bool Edge::getValue () const {
    return value;
}

bool Edge::setValue (bool newValue)
{
    if( value == newValue ) {
        return false;
    }
    value = newValue;
    for( int i=0; i<(int) outputs.size(); i++ ) {
        pair<string,Gate*> p = outputs[i];
        p.second->handleSignal ();
    }
    return true;
}
