#include "../../headers/core/GateContainer.h"
#include "../../headers/core/structures/GateStructure.h"
#include "../../headers/core/dynamics/DynamicGateType.h"
#include "../../headers/core/statics/StaticGateType.h"
#include "../../headers/core/Context.h"

using namespace std;

GateContainer::GateContainer() {
}

GateContainer::~GateContainer() {
	int size = gateStructures.size();
    for( int i = 0 ; i < size ; i++ ) {
        delete gateStructures[i];
    }
    size = usedTypes.size();
    for( int i = 0 ; i < size ; i++ ) {
        delete usedTypes[i];
    }
}

vector<GateStructure*>& GateContainer::getGateStructures() {
    return gateStructures;
}

vector<GateStructure*> GateContainer::getInternalInput(string name) {
    return internalInputs[name];
}

GateStructure* GateContainer::getInternalOutput (string name) {
    return internalOutputs[name];
}

DynamicGateType* GateContainer::createDynamicGateType(vector<string> inputNames, vector<string> outputNames){
	DynamicGateType* newGateType = new DynamicGateType (inputNames, outputNames, &context);
	usedTypes.push_back(newGateType);
	return newGateType;
}

StaticGateType* GateContainer::createStaticGateType(vector<string> inputNames, vector<string> outputNames){
	StaticGateType* newGateType = new StaticGateType (inputNames, outputNames, &context);
	usedTypes.push_back(newGateType);
	return newGateType;
}

GateStructure* GateContainer::addGateStructure(GateType* type) {
	GateStructure* gateStructure = new GateStructure(type);
	gateStructure->setId(gateStructures.size());
	// invariant utilisé par DynamicGateType::addGateStructure : c'est aussi l'index dans le vecteur
	gateStructures.push_back(gateStructure);
	return gateStructure;
}

void GateContainer::addInput (string inputName, GateStructure* inputGate) {
    internalInputs[inputName].push_back(inputGate);
}

void GateContainer::setOutput (string outputName, GateStructure* outputGate) {
	internalOutputs[outputName] = outputGate;
}
