#include <vector>
#include <map>
#include <string>
#include <algorithm>

#include "../../headers/core/Gate.h"
#include "../../headers/core/GateType.h"
#include "../../headers/core/Edge.h"
#include "../../headers/core/memories/RegGate.h"
#include "../../headers/core/memories/RamGate.h"
#include "../../headers/core/statics/StaticGate.h"
#include "../../headers/core/statics/StaticGateType.h"
#include "../../headers/core/dynamics/DynamicGateType.h"
#include "../../headers/core/dynamics/DynamicGate.h"

using namespace std;

Gate::Gate (GateType* gateType) : signal(true), outputEdges(), inputEdges() {
    if (! gateType->isFinished()) {
        throw GateLibException("Unfinished GateType has been instanciated in a Gate");
    }

    vector<string> outputNames = gateType->getOutputNames();
    for( int i=0; i<(int) outputNames.size(); i++ ) {
        outputEdges[ outputNames[i] ] = new Edge();
    }
}

Gate::~Gate() {

    map<string, Edge*>::iterator
		itBegin = outputEdges.begin(),
		itEnd = outputEdges.end();

	for (; itBegin != itEnd ; itBegin++) {
		delete itBegin->second;
	}
}

// Pas int�gr� dans le constructeur pour permettre la cr�ation de boucles dans les circuits
void Gate::setInputEdge (string name, Edge* edge) throw(InvalidNameException) {
    // V�rification de la signature
    vector<string> inputNames = getType()->getInputNames();
    vector<string>::iterator
        itBegin = inputNames.begin(),
        itEnd = inputNames.end();
    if (find (itBegin, itEnd, name) == itEnd) {
        throw InvalidNameException ( name);
    }
    inputEdges[name] = edge;
}

Edge* Gate::getInputEdge (string name) {
    return inputEdges[name];
}

Edge* Gate::getOutputEdge (string name) {
	return outputEdges[name];
}

map<string, Edge*> Gate::getInputEdges () {
	return inputEdges;
}

map<string, Edge*> Gate::getOutputEdges () {
	return outputEdges;
}

void Gate::handleSignal () {
    signal = true;
}

void Gate::clearSignal() {
	signal = false;
}

bool Gate::hasSignal() {
	return signal;
}

Gate* Gate::create (GateType* type) throw(GateLibException) {
	switch (type -> getMetaType()) {
		case STATIC :
			return new StaticGate((StaticGateType*) type);
			break;

		case REG :
			return new RegGate();
			break;

		case DYNAMIC :
			return new DynamicGate((DynamicGateType*) type);
			break;

        case RAM :
            return new RamGate ((RamGateType*) type);
            break;

		default :
			throw GateLibException("Unknown meta type");
	}
}
