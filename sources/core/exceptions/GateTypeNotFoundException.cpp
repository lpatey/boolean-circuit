

#include "../../../headers/core/exceptions/GateTypeNotFoundException.h"

using namespace std;

GateTypeNotFoundException::GateTypeNotFoundException(string name) :
    GateLibException( "Invalid gate type : " + name ), name(name) {
}

string GateTypeNotFoundException::getName() const {
    return name;
}
