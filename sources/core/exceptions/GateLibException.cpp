
#include "../../../headers/core/exceptions/GateLibException.h"

using namespace std;

GateLibException::GateLibException(string message):message(message) {
}

string GateLibException::getMessage() const {
    return message;
}
