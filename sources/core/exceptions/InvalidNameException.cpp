
#include "../../../headers/core/exceptions/InvalidNameException.h"

using namespace std;

InvalidNameException::InvalidNameException(string name) :
    GateLibException( "Invalid name : " + name ), name(name) {
}

string InvalidNameException::getName() const {
    return name;
}
