
#include "../../../headers/core/exceptions/CombinatoryCycleException.h"
#include "../../../headers/core/structures/EdgeStructure.h"

using namespace std;

CombinatoryCycleException::CombinatoryCycleException(EdgeStructure* edgeStructure) :
    GateLibException("Combinatory cycle detected in " + edgeStructure->getInput().first),
    edgeStructure(edgeStructure) {
}

EdgeStructure* CombinatoryCycleException::getEdgeStructure() const {
    return edgeStructure;
}
