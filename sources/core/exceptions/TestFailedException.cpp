
#include "../../../headers/core/exceptions/TestFailedException.h"

using namespace std;
using namespace token;

TestFailedException::TestFailedException(TestStatementToken test, map<string,bool> values, int index)
    : GateLibException( "Test failed for " + test.identifier.getName() ), test(test), values(values), index(index) {
}

TestStatementToken& TestFailedException::getTest() {
    return test;
}

map<string,bool>& TestFailedException::getValues() {
    return values;
}

int TestFailedException::getIndex() {
    return index;
}
