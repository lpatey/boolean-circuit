
#include "../../../headers/core/exceptions/TupleDimensionException.h"

using namespace std;

TupleDimensionException::TupleDimensionException(string first, string second, string context)
    : GateLibException( "Tuple dimension doesn't match between " + first + " and " + second + " in " + context ) {
}
