
#include "../../../headers/core/algorithms/TopologicalSorter.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/structures/GateStructure.h"
#include "../../../headers/core/structures/EdgeStructure.h"

#include <limits>
#include <map>

using namespace std;

TopologicalSorter::TopologicalSorter(DynamicGateType* type) :
        todo(),
        type(type),
        depthThreshold(0) {

}

void TopologicalSorter::compute() throw(CombinatoryCycleException) {

    map<string,EdgeStructure*> internalInputs = getType()->getInternalInputs();
    for( map<string,EdgeStructure*>::iterator it = internalInputs.begin(); it != internalInputs.end(); ++it ) {
        todo.insert( it->second );
    }
    vector<GateStructure*> gateStructures = getType()->getGateStructures();
    for( int i=0; i<(int) gateStructures.size(); i++ ) {
        GateStructure* gate = gateStructures[i];
        map<string,EdgeStructure*> outputs = gate->getOutputs();
        for( map<string,EdgeStructure*>::iterator eit = outputs.begin(); eit != outputs.end(); ++eit ) {
            todo.insert( eit->second );
        }
    }
    depthThreshold = todo.size();

    while( !todo.empty() ) {
        computeNextEdge();
    }

    computeGateDepthsFromEdgeDepths();

}

void TopologicalSorter::computeNextEdge() throw(CombinatoryCycleException) {
    EdgeStructure* edge = *(todo.begin());
    todo.erase( edge );
    int depth = edge->getDepth();

    if( depth > depthThreshold ) {
        throw CombinatoryCycleException( edge );
    }

    vector<pair<string,GateStructure*> > edgeOutputs = edge->getOutputs();
    for( int i=0; i<(int) edgeOutputs.size(); i++ ) {
        pair<string,GateStructure*> from = edgeOutputs[i];
        GateStructure* gate = from.second;

        map<string,EdgeStructure*> gateOutputs = gate->getOutputs();
        for( map<string,EdgeStructure*>::iterator to = gateOutputs.begin(); to != gateOutputs.end(); ++to ) {
            EdgeStructure* nextEdge = to->second;
            bool isCombinatory = gate->getType()->connectionType(from.first, to->first)
				== COMBINATORY;
            if( !isCombinatory ) {
                continue;
            }
            if( nextEdge->getDepth() < depth + 1 ) {
                nextEdge->setDepth( depth + 1 );
                todo.insert( nextEdge );
            }
        }
    }

}


void TopologicalSorter::computeGateDepthsFromEdgeDepths() {
    vector<GateStructure*> vect = getType()->getGateStructures();
    for( int i=0; i<(int) vect.size(); i++ ) {
        GateStructure* gateStructure = vect[i];

        int minDepth = numeric_limits<int>::max();
        map<string,EdgeStructure*> outputs = gateStructure->getOutputs();

        for(map<string,EdgeStructure*>::iterator eit = outputs.begin(); eit != outputs.end(); ++eit ) {
            EdgeStructure* edge = eit->second;
            if (edge->getDepth() < minDepth) {
                minDepth = edge->getDepth();
            }
        }
        gateStructure->setDepth( minDepth );
    }
}


DynamicGateType* TopologicalSorter::getType() const {
    return type;
}

int TopologicalSorter::getDepthThreshold() const {
    return depthThreshold;
}
