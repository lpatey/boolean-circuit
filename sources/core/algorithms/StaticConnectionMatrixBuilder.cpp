#include "../../../headers/core/algorithms/StaticConnectionMatrixBuilder.h"
#include "../../../headers/core/statics/StaticGateType.h"

#include <algorithm>

using namespace std;

StaticConnectionMatrixBuilder::StaticConnectionMatrixBuilder(StaticGateType* type) :
        type(type), iNames(type->getInputNames()), oNames(type->getOutputNames()),
        iSize(0), oSize(0), truthTable(type->getTruthTable()) {

    iSize = iNames.size();
    oSize = oNames.size();
}

StaticGateType* StaticConnectionMatrixBuilder::getType () const {
    return type;
}

void StaticConnectionMatrixBuilder::compute () {

    for (int i = 0 ; i < iSize ; i++) {
        vector<string> inputNames = iNames;
        string inputName = iNames[i];

        inputNames.erase(find(inputNames.begin(),inputNames.end(),inputName));

        for (int j = 0 ; j < oSize ; j++) {
            map<string,bool> notExpanded;
            if (areConnected(inputNames, notExpanded, inputName, oNames[j] ) ) {
                type->declareConnected(inputName, oNames[j]);
            }
        }
    }
}

bool StaticConnectionMatrixBuilder::areConnected(
            vector<string> notExpanded,
            map<string,bool> expanded, string inputName, string outputName ) {

    if( notExpanded.size() != 0 ) {
        string input = *notExpanded.begin();
        notExpanded.erase( find(notExpanded.begin(), notExpanded.end(), input ) );

        expanded[input] = true;
        if( areConnected( notExpanded, expanded, inputName, outputName ) ) {
            return true;
        }

        expanded[input] = false;
        if( areConnected( notExpanded, expanded, inputName, outputName ) ) {
                return true;
        }

        return false;

    }


    expanded[inputName] = true;
    map<string,bool> value1 = truthTable.getValue(expanded);
    expanded[inputName] = false;
    map<string,bool> value2 = truthTable.getValue(expanded);

    return value1[outputName] != value2[outputName];

}
