#include "../../../headers/core/algorithms/ConnectionMatrixBuilder.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/structures/EdgeStructure.h"
#include "../../../headers/core/structures/GateStructure.h"

#include <map>

using namespace std;

ConnectionMatrixBuilder::ConnectionMatrixBuilder(DynamicGateType* type) :
        todo(), connectedEdges(), combinatoryConnectedEdges(),
        currentInternalInputName(), type(type) {

}

void ConnectionMatrixBuilder::compute() {

    map<string, EdgeStructure*> internalInputs = getType()->getInternalInputs();
    map<string, EdgeStructure*>::iterator
		source = internalInputs.begin(),
		end = internalInputs.end();
    for( ; source != end ; ++source) {
    	currentInternalInputName = source->first;
        todo.insert(pair<ConnectionType,EdgeStructure*>(COMBINATORY, source->second));
        connectedEdges.insert(source->second);
        combinatoryConnectedEdges.insert(source->second);

        // Parcours du graphe
        while(!todo.empty()) {
            computeNextSource();
        }

        // Transmission des r�sultats � type
        map<string, EdgeStructure*> internalOutputs = type->getInternalOutputs();
        map<string, EdgeStructure*>::iterator
			ioit = internalOutputs.begin(),
			endio = internalOutputs.end();
		for( ; ioit != endio ; ++ioit ) {

			if(connectedEdges.count(ioit->second)) {
				type->declareConnected(currentInternalInputName, ioit->first);
				set<EdgeStructure*>::iterator itCombinatory = combinatoryConnectedEdges.find(ioit->second);
				if(combinatoryConnectedEdges.count(ioit->second)) {
					type->declareCombinatory(currentInternalInputName, ioit->first);
				}
			}
		}
    }

}

void ConnectionMatrixBuilder::computeNextSource() {
	set<pair<ConnectionType, EdgeStructure*> >::iterator iter = todo.begin();
    EdgeStructure* edge = iter->second;
    ConnectionType cType = iter->first;
    todo.erase(iter);

    vector<pair<string, GateStructure*> > edgeOutputs = edge->getOutputs();
    for( int i=0; i<(int) edgeOutputs.size(); i++ ) {
        pair<string, GateStructure*> from = edgeOutputs[i];
        GateStructure* gate = from.second;
        map<string, EdgeStructure*> gateOutputs = gate->getOutputs();
        map<string,EdgeStructure*>::iterator
			to = gateOutputs.begin(),
			end = gateOutputs.end();
        for( ; to != end; ++to) {
            computeNextEntry(gate->getType(),
            		from.first, cType, to->first, to->second);
        }
    }
}

void ConnectionMatrixBuilder::computeNextEntry(GateType* gateType,
    string inputName, ConnectionType inputStatus, string outputName, EdgeStructure* output ) {

	ConnectionType cType = gateType->connectionType( inputName, outputName);
    if(cType == NONE) {
    }
    else if (cType == MEMORY) {
         if (connectedEdges.count(output)) {
            return ;
        }
    	todo.insert(pair<ConnectionType,EdgeStructure*>(MEMORY, output));
    	connectedEdges.insert(output);
    }
    else { // cType == COMBINATORY
    	if (inputStatus == MEMORY) {
    	    if (connectedEdges.count(output)) {
    	        return ;
    	    }
    		todo.insert(pair<ConnectionType,EdgeStructure*>(MEMORY, output));
    		connectedEdges.insert(output);
    	}
    	else { // inputStatus == COMBINATORY, car NONE pas possible puisque dans todo
    	    if (combinatoryConnectedEdges.count(output)) {
    	        return ;
    	    }
    		todo.insert(pair<ConnectionType,EdgeStructure*>(COMBINATORY, output));
    		connectedEdges.insert(output);
    		combinatoryConnectedEdges.insert(output);
    	}

    }
}

DynamicGateType* ConnectionMatrixBuilder::getType() const {
    return type;
}
