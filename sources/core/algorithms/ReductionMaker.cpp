#include "../../../headers/core/algorithms/ReductionMaker.h"
#include "../../../headers/core/statics/StaticGateType.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/structures/EdgeStructure.h"
#include "../../../headers/core/structures/GateStructure.h"

#include <stack>
#include <set>

using namespace std;

ReductionMaker::ReductionMaker (StaticGateType* type) :
		sType(type), dType(0), metaType(STATIC), gateContainer(type->getGateContainer()),
		iNames(type->getInputNames()), oNames(type ->getOutputNames()) {
    iSize = iNames.size();
    oSize = oNames.size();
}

ReductionMaker::ReductionMaker (DynamicGateType* type):
		sType(0), dType(type), metaType(DYNAMIC), gateContainer(type->getGateContainer()),
		iNames(type->getInputNames()), oNames(type ->getOutputNames()) {
    iSize = iNames.size();
    oSize = oNames.size();
}

bool ReductionMaker::isIrreductible (GateType* type) {
    for (int i = 0 ; i < iSize ; i++) {
        bool combinatory = type->connectionType(iNames[i], oNames[0]) == COMBINATORY;
        for (int j = 1 ; j < oSize ; j++) {
            if (combinatory != (type->connectionType(iNames[i], oNames[j]) == COMBINATORY)) {
                return false;
            }
        }
    }
    return true;
}

void ReductionMaker::wrapIrreductibleGS (GateType* type) {
    GateStructure* uniqueGS = gateContainer->addGateStructure(type);
    for (int i = 0 ; i < oSize ; i++) {
        gateContainer->setOutput(oNames[i], uniqueGS);
    }
    for (int i = 0 ; i < iSize ; i++) {
        gateContainer->addInput(iNames[i], uniqueGS);
    }
}

void ReductionMaker::connectIntoGateContainer (GateType* newType,
        vector<string> dependentInputs, int nbrDI,
        vector<string> treatedOutputs, int nbrTO) {

    GateStructure* structure = gateContainer->addGateStructure(newType);
    for (int i = 0 ; i < nbrDI ; i++) {
        gateContainer->addInput(dependentInputs[i], structure);
    }
    for (int i = 0 ; i < nbrTO ; i++) {
        gateContainer->setOutput(treatedOutputs[i], structure);
    }
}

bool ReductionMaker::compute() throw(GateLibException){
    switch(metaType) {
        case STATIC :
            return computeStatic();
            break;

        case DYNAMIC :
            return computeDynamic();
            break;

        default :
            throw GateLibException
                ("Reduction::compute : MetaType invalide.");
    }
}

bool ReductionMaker::computeStatic() {
	if (isIrreductible(sType)) {
		wrapIrreductibleGS(sType);
		return true;
	}

    set<string> variablesInput;
    for (int i = 0 ; i < iSize ; i++) {
        variablesInput.insert(iNames[i]);
    }

    for (int i = 0 ; i < oSize ; i++) {
        string curOName = oNames[i];
        set<string> variableOutput;
        vector<string> singletonOutput;
        singletonOutput.push_back (curOName);
        variableOutput.insert(curOName);

        StaticGateType* subType = gateContainer->createStaticGateType(iNames, singletonOutput);
        BinaryTree oldTruthTable = sType->getTruthTable();
        BinaryTree newTruthTable = subType->getTruthTable();
        oldTruthTable.copyExtract(variablesInput, variableOutput, &newTruthTable);
        subType->compute();

        connectIntoGateContainer(subType, iNames, iSize, singletonOutput, 1);
	}

	return false;
}

void ReductionMaker::copyInternalInputs (DynamicGateType* oldType, DynamicGateType* newType,
        map<GateStructure*,GateStructure*> newGS) {

    map<string, EdgeStructure*> newInternalInputs = newType->getInternalInputs();
    map<string, EdgeStructure*>::iterator
        itNewInternalInputs = newInternalInputs.begin(),
        itNewInternalInputsEnd = newInternalInputs.end();
    for (; itNewInternalInputs != itNewInternalInputsEnd ; ++itNewInternalInputs) {
        string inputName = itNewInternalInputs->first;
        EdgeStructure* oldEdge = oldType->getInternalInput(inputName);
        EdgeStructure* newEdge = itNewInternalInputs->second;

        vector<pair<string, GateStructure*> > oldNextGates= oldEdge->getOutputs();
        int oldNextGatesSize = oldNextGates.size ();
        for (int i = 0 ; i < oldNextGatesSize ; i++) {
            GateStructure* newNextGate = newGS[oldNextGates[i].second];
            if (newNextGate != 0) {
                newEdge->addOutput(oldNextGates[i].first, newNextGate);
            }
        }
    }
}

void ReductionMaker::copyInternalOutputs (DynamicGateType* oldType, DynamicGateType* newType,
        map<GateStructure*, GateStructure*> newGS) {

    vector<string> treatedOutputs = newType->getOutputNames();
    int nbrTO = treatedOutputs.size();
    for (int i = 0 ; i < nbrTO ; i++) {
        EdgeStructure* oldEdge = oldType->getInternalOutput(treatedOutputs[i]);
        pair<string, GateStructure*> oldInput = oldEdge->getInput();
        string oldOutputName = oldInput.first;
        GateStructure* oldGate = oldInput.second;
        newType->addInternalOutput(newGS[oldGate]->getOutput(oldOutputName), treatedOutputs[i]);
    }
}

bool ReductionMaker::computeDynamic() {

	if (isIrreductible(dType)) {
        wrapIrreductibleGS(dType);
        return true;
    }

    for (int i = 0 ; i < oSize ; i++) {
        vector<string> singletonOutput;
        string curOName = oNames[i];
        singletonOutput.push_back (curOName);
        // Stockage des variables d'entrées dans un set
        set<EdgeStructure*> internalInputEdges;
        map<string, EdgeStructure*> internalInputEdgesMap = dType->getInternalInputs();
        map<string, EdgeStructure*>::iterator
            itIIEM = internalInputEdgesMap.begin(),
            itIIEMEnd = internalInputEdgesMap.end();
        for ( ; itIIEM != itIIEMEnd ; ++itIIEM) {
            internalInputEdges.insert(itIIEM->second);
        }

        DynamicGateType* subType = gateContainer->createDynamicGateType(iNames, singletonOutput);

        map<GateStructure*, GateStructure*> newGS;

        set<EdgeStructure*> done;
        stack<EdgeStructure*> todo;
        EdgeStructure* outputEdge = dType->getInternalOutput(curOName);
        todo.push(outputEdge);

        // Ajout des sous-sous-portes
        while (!todo.empty()) {
            EdgeStructure* edge = todo.top();
            todo.pop();
            if (internalInputEdges.count(edge) || done.count(edge)) {
                continue;
            }
            pair<string, GateStructure*> edgeInput = edge->getInput();
            GateStructure* oldGate = edgeInput.second;
            // Irréductible, puisque tiré d'un GateStructure
            GateStructure* newGate = subType->addGateStructure(oldGate->getType()).outputs.begin()->second;
            newGS[oldGate] = newGate;
        }

        // Ajout du cablage
        set<EdgeStructure*>::iterator itDone = done.begin(), itDoneEnd = done.end();
        for ( ; itDone != itDoneEnd ; ++itDone) {
            EdgeStructure* edge = *itDone;
            pair<string, GateStructure*> edgeInput = edge->getInput();
            GateStructure* oldInputGate = edgeInput.second;
            GateStructure* newInputGate = newGS[oldInputGate];
            EdgeStructure* newEdge = newInputGate->getOutput(edgeInput.first);

            vector<pair<string, GateStructure*> > edgeOutputs = edge->getOutputs();
            int edgeOutputsSize = edgeOutputs.size();
            for (int j = 0 ; j < edgeOutputsSize ; j++) {
                newEdge->addOutput(edgeOutputs[j].first, newGS[edgeOutputs[j].second]);
            }
        }

        copyInternalInputs(dType, subType, newGS);
        copyInternalOutputs(dType, subType, newGS);
        subType->compute();

        connectIntoGateContainer(subType, iNames, iSize, singletonOutput, 1);
    }
    return false;
}
