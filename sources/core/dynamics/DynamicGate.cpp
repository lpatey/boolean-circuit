#include <sstream>

#include "../../../headers/core/dynamics/DynamicGate.h"
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/structures/GateStructure.h"
#include "../../../headers/core/Edge.h"
#include "../../../headers/core/memories/MemoryBit.h"
#include "../../../headers/core/structures/EdgeStructure.h"

using namespace std;

DynamicGate::DynamicGate (DynamicGateType* type) :
	Gate((GateType*) type), internalInputEdges(), internalOutputEdges(),
    memBits(), subGates(), type(type) {

    initSubGates();
    initInternalInputs();
    initInternalOutputs();
    linkDirectEdges();
    initEdges();
    initMemBits();
}

void DynamicGate::initSubGates() {
    vector<GateStructure*> structs = type->getGateStructures ();
    int sizeStructs = structs.size();
    for (int i = 0 ; i < sizeStructs ; i++) {
        int depth = structs[i]->getDepth();
        while( ((int) subGates.size()) < (depth +1) ) {
            subGates.push_back(vector<Gate*>());
        }
        while( ((int) subGates[depth].size()) < (structs[i]->getId() +1) ) {
            subGates[depth].push_back(0);
        }

        subGates[depth][structs[i]->getId()] = Gate::create(structs[i]->getType());
    }
}

void DynamicGate::linkDirectEdges () {
    map<string,EdgeStructure*>&
        internalInputs = type->getInternalInputs(),
        internalOutputs = type->getInternalOutputs();
    map<string,EdgeStructure*>::iterator
        inputIt = internalInputs.begin(),
        outputIt = internalOutputs.begin();

    for( ; inputIt != internalInputs.end(); ++inputIt ) {
        for( ; outputIt != internalOutputs.end(); ++outputIt ) {
            if( inputIt->second == outputIt->second ) {
                internalOutputEdges[outputIt->first] = internalInputEdges[inputIt->first];
            }
        }
    }
}

void DynamicGate::initInternalInputs () {

    vector<string> iNames = type->getInputNames();
    int iSize = iNames.size();
    for (int i = 0 ; i < iSize ; i++) {
        string curIName = iNames[i];
        internalInputEdges[curIName] = new Edge ();
        EdgeStructure* edgeStruct = type->getInternalInput(curIName);
        mapStructToEdges(edgeStruct, internalInputEdges[curIName]);
    }
}

void DynamicGate::initInternalOutputs() {
    vector<string> oNames = type->getOutputNames();
    int oSize = oNames.size();
    for (int i = 0 ; i < oSize ; i++) {
        string curOName = oNames[i];
        EdgeStructure* curEdgeStruct = type->getInternalOutput(curOName);
        pair<string, GateStructure*> pairStrGS = curEdgeStruct->getInput();
        if( pairStrGS.second == 0 ) {
            continue;
        }
        Gate* gateFrom = getGateFromStruct(pairStrGS.second);
        Edge* edge = gateFrom->getOutputEdge(pairStrGS.first);
        internalOutputEdges[curOName] = edge;
    }
}

void DynamicGate::initEdges () {

    vector<GateStructure*> structs = type->getGateStructures ();
    int sizeStructs = structs.size();
    for (int i = 0 ; i < sizeStructs ; i++) {
        Gate* gateFrom = getGateFromStruct(structs[i]);
        map<string, EdgeStructure*> nextEdges = structs[i]->getOutputs();
        map<string, EdgeStructure*>::iterator
            itNextEdge = nextEdges.begin(),
            itNextEdgeEnd = nextEdges.end();
        for ( ; itNextEdge != itNextEdgeEnd ; ++itNextEdge) {
            mapStructToEdges(itNextEdge->second, gateFrom->getOutputEdge(itNextEdge->first));
        }
    }
}

void DynamicGate::initMemBits () {
    int depth = 0;
    int size = subGates.size();
    for (int i = 0 ; i < size ; i++) {
        int id = 0;
        int subSize = subGates[i].size();
        for (int j = 0 ; j < subSize ; j++) {
            ostringstream oss;
            oss << depth << "." << id;
            string gateLabel = oss.str();
            map<string, MemoryBit*> mBs = subGates[i][j]->getMemoryBits();
            map<string, MemoryBit*>::iterator
                itMB = mBs.begin(),
                itMBEnd = mBs.end();
            for ( ; itMB != itMBEnd ; ++itMB) {
                memBits[gateLabel + itMB->first] = itMB->second;
            }
            id++;
        }
        depth++;
    }
}

void DynamicGate::mapStructToEdges (EdgeStructure* edgeStruct, Edge* edge) {
	vector<pair<string, GateStructure*> > outputs = edgeStruct->getOutputs();
	int size = outputs.size();

	for (int i = 0 ; i < size ; i++) {
		GateStructure* gateStruct = outputs[i].second;
		Gate* gateTo = getGateFromStruct(gateStruct);
		edge->addOutput(outputs[i].first, gateTo);
	}
}

Gate* DynamicGate::getGateFromStruct (GateStructure* gateStruct) {
	return subGates[gateStruct->getDepth()][gateStruct->getId()];
}

DynamicGate::~DynamicGate() {
	map<string, EdgeStructure*> iInputs = type->getInternalInputs();
	map<string, EdgeStructure*>::iterator
		itIInput = iInputs.begin(),
		itIInputEnd = iInputs.end();
	for ( ; itIInput != itIInputEnd ; ++itIInput) {
		delete itIInput->second;
	}
}

Edge* DynamicGate::getInternalInputEdge (string name) {
	return internalInputEdges[name];
}

Edge* DynamicGate::getInternalOutputEdge (string name) {
	return internalOutputEdges[name];
}

map<string, Edge*>& DynamicGate::getInternalInputEdges () {
	return internalInputEdges;
}

map<string, Edge*>& DynamicGate::getInternalOutputEdges () {
	return internalOutputEdges;
}

map<string, MemoryBit*> DynamicGate::getMemoryBits () {
	return memBits;
}

GateType* DynamicGate::getType() const {
    return (GateType*) type;
}

void DynamicGate::tick (bool b) {
	if (!hasSignal() && memBits.empty()){
		return ;
	}
	if (type->isCacheEnabled()) {
		map<string, bool> entries = getEntries ();
		BinaryTree binaryTree = type->getTruthTable();
		map<string, bool> result = binaryTree.getValue (entries);
		if (! result.empty()) {
			refreshMemories (result);
			emitOutput (result);
			return ;
		}
		result = compute(b);
		addMemories(&result);
		binaryTree.setValue(entries, result);
		return ;
	}
	compute (b);
}

void DynamicGate::emitOutput (map<string,bool> result) {
	vector<string> oNames = type->getOutputNames();
	int size = oNames.size();
	for (int i = 0 ; i < size ; i++) {
		outputEdges[oNames[i]]->setValue(result[oNames[i]]);
	}
}

void DynamicGate::refreshMemories (map<string,bool> result) {
	map<string, MemoryBit*>::iterator
		itMemBit = memBits.begin(),
		itMemBitEnd = memBits.end();
	for ( ; itMemBit != itMemBitEnd ; ++itMemBit) {
		itMemBit->second->setValue(result[itMemBit->first]);
	}
}

map<string, bool> DynamicGate::compute (bool b) {
	vector<string> iNames = type->getInputNames();
	int iSize = iNames.size();
	for (int i = 0 ; i < iSize ; i++) {
		internalInputEdges[iNames[i]]->setValue(inputEdges[iNames[i]]->getValue());
	}

	int gSize = subGates.size();
	for (int i = 0 ; i < gSize ; i++) {
		int subSize = subGates[i].size();
		for (int j = 0 ; j < subSize ; j++) {
			subGates[i][j]->tick(b);
		}
	}

	map<string, bool> result;
	vector<string> oNames = type->getOutputNames();
	int oSize = oNames.size();
	for (int i = 0 ; i < oSize ; i++) {
	    string curOName = oNames[i];
	    bool oValue = internalOutputEdges[curOName]->getValue();
	    result[curOName] = oValue;
        outputEdges[curOName]->setValue(oValue);
	}
	return result;
}

void DynamicGate::addMemories(map<string, bool>* resultAdress) {
    map<string, bool> result = *resultAdress;
	map<string, MemoryBit*>::iterator
		itMemBits = memBits.begin(),
		itMemBitsEnd = memBits.end();
	for ( ; itMemBits != itMemBitsEnd ; ++itMemBits) {
		result[itMemBits->first] = itMemBits->second->getValue();
	}
}

map<string, bool> DynamicGate::getEntries () {
	map<string, bool> entries;

    map<string, Edge*>::iterator
        itInputEdge = inputEdges.begin(),
        itInputEdgeEnd = inputEdges.end();
    for ( ; itInputEdge != itInputEdgeEnd ; ++itInputEdge) {
        entries[itInputEdge->first] = itInputEdge->second->getValue();
    }

	map<string, MemoryBit*>::iterator
		itMemBit = memBits.begin(),
		itMemBitEnd = memBits.end();
	for ( ; itMemBit != itMemBitEnd ; ++itMemBit) {
		entries[itMemBit->first] = itMemBit->second->getValue();
	}

	return entries;
}
