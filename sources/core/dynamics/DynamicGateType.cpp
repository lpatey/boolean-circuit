
#include "../../../headers/core/dynamics/DynamicGateType.h"
#include "../../../headers/core/structures/EdgeStructure.h"
#include "../../../headers/core/structures/GateStructure.h"
#include "../../../headers/core/algorithms/TopologicalSorter.h"
#include "../../../headers/core/algorithms/ConnectionMatrixBuilder.h"
#include "../../../headers/core/algorithms/ReductionMaker.h"

#include <algorithm>

using namespace std;

DynamicGateType::DynamicGateType(vector<string> inputNames, vector<string> outputNames, Context* context) :
    GateType(inputNames,outputNames,context), gateStructures(), cacheEnabled(false), gateContainer(),
    connectionMatrix(), internalInputs(), internalOutputs() {

    for( int i=0; i<(int) inputNames.size(); i++ ) {
        string inputName = inputNames[i];

        for( int j=0; j<(int) outputNames.size(); j++ ) {
            string outputName = outputNames[j];
            connectionMatrix[ pair<string,string>( inputName, outputName ) ] = NONE;
        }
    }

    for( int i=0; i<(int) inputNames.size(); i++ ) {
        internalInputs[ inputNames[i] ] = new EdgeStructure();
    }

}

DynamicGateType::~DynamicGateType() {
    map<string,EdgeStructure*>::iterator it = internalInputs.begin();
    for( ; it != internalInputs.end(); ++it ) {
        delete it->second;
    }

    for( int i=0; i<(int) gateStructures.size(); i++ ) {
        delete gateStructures[i];
    }
}

GateStructuresMapping DynamicGateType::addGateStructure (GateType* type) {

	GateContainer* gateContainerAdd = type->getGateContainer();

	GateStructuresMapping mapping;
	// Ajout de(s) sous-porte(s)
	vector<GateStructure*> gSAdd = gateContainerAdd->getGateStructures();
	vector<GateStructure*> gSAdded; // temporaire, sert à garder un accès aux nouvelles GateStructure-s
	int gSSize = gSAdd.size();
	for (int i = 0 ; i < gSSize ; i++) {
		GateStructure* nGS = new GateStructure(gSAdd[i]->getType());
		gSAdded.push_back( nGS );
		gateStructures.push_back (nGS);
	}
    /* Disabled dans la version courante car
    * l'implémentation actuelle de ReductionMaker ne rajoute pas de connections internes au GateContainer.
	// Copie des connections internes (si présentes ..)
	for (int i = 0 ; i < gSSize ; i++) {
		map<string, EdgeStructure*> outputEdgesToAdd = gSAdd[i]->getOutputs ();
		map<string, EdgeStructure*>::iterator
			itOutput = outputEdgesToAdd.begin(),
			itOutputEnd = outputEdgesToAdd.end();
		for ( ; itOutput != itOutputEnd ; ++itOutput) {
			vector<pair<string, GateStructure*> > toGates = itOutput->second->getOutputs();
			int toGatesSize = toGates.size();
			for (int j = 0 ; j < toGatesSize ; i++) {
				gSAdded[i]->getOutput(itOutput->first)->addOutput(toGates[i]->first, toGates[i]->second);
			}
		}
	}
    */
	// Enregistrement de(s) entrée(s)
	vector<string> inputNamesAdd = type->getInputNames();
	int inputSize = inputNamesAdd.size();
	map<string, vector<GateStructure*> > result;
	for (int i = 0 ; i < inputSize ; i++) {
		vector<GateStructure*> oldInputGS = gateContainerAdd->getInternalInput(inputNamesAdd[i]);
		vector<GateStructure*> newInputGS;
		int size = oldInputGS.size();
		for (int j = 0 ; j < size ; j++) {
			newInputGS.push_back(gSAdded[oldInputGS[j]->getId()]);
		}
		mapping.inputs[inputNamesAdd[i]] = newInputGS;
	}

	// Enregistrement de(s) sortie(s)
	const vector<string>& outputNamesAdd = type->getOutputNames();
	int outputSize = outputNamesAdd.size();
	for (int i = 0 ; i < outputSize ; i++) {
	    string outputName = outputNamesAdd[i];
		GateStructure* structure = gateContainerAdd->getInternalOutput(outputName);
		mapping.outputs[outputName] = gSAdded[structure->getId()];
	}

	return mapping;
}



void DynamicGateType::addInternalOutput (EdgeStructure* output, string name) throw(InvalidNameException) {
    if( find( outputNames.begin(), outputNames.end(), name ) == outputNames.end() ) {
        throw InvalidNameException( name );
    }
    internalOutputs[name] = output;
}

vector<GateStructure*>& DynamicGateType::getGateStructures() {
    return gateStructures;
}

int DynamicGateType::getMemoryBitsNumber() {
    int num = 0;
    int size = gateStructures.size();
    for (int i = 0 ; i < size ; i++) {
        num += gateStructures[i]->getType()->getMemoryBitsNumber();
    }
    return num;
}

void DynamicGateType::compute() throw(CombinatoryCycleException, GateLibException) {
    GateType::compute();
	TopologicalSorter sorter(this);
	sorter.compute();
	ConnectionMatrixBuilder builder(this);
    builder.compute();
    ReductionMaker maker(this);
    maker.compute();
    computeGateStructureIds();
}

EdgeStructure* DynamicGateType::getInternalInput( string name ) {
    return internalInputs[ name ];
}

map<string, EdgeStructure*>& DynamicGateType::getInternalInputs() {
    return internalInputs;
}

EdgeStructure* DynamicGateType::getInternalOutput (string name) {
    return internalOutputs[ name ];
}

map<string, EdgeStructure*>& DynamicGateType::getInternalOutputs() {
    return internalOutputs;
}

BinaryTree& DynamicGateType::getTruthTable() {
    return truthTable;
}

bool DynamicGateType::isCacheEnabled() const {
    return cacheEnabled;
}

void DynamicGateType::setCacheEnabled (bool flag) {
    cacheEnabled = flag;
}

void DynamicGateType::declareConnected(string input, string output) {
    connectionMatrix[pair<string, string>(input, output)] = MEMORY;
}


void DynamicGateType::declareCombinatory(string input, string output) {
    connectionMatrix[pair<string, string>(input, output)] = COMBINATORY;
}

ConnectionType DynamicGateType::connectionType( string input, string output ) {
    return connectionMatrix[ pair<string, string>( input, output ) ];
}

void DynamicGateType::computeGateStructureIds() {

    vector<int> maxIds;

    for( int i=0; i<(int)gateStructures.size(); i++ ) {
        GateStructure* gateStructure = gateStructures[i];

        int depth = gateStructure->getDepth();
        while ( ((int)maxIds.size()) < (depth + 1) ) {
            maxIds.push_back(0);
        }
        int id = maxIds[depth];
        gateStructure->setId(id);
        maxIds[depth] = id + 1;
    }
}

MetaType DynamicGateType::getMetaType() {
    return DYNAMIC;
}

GateContainer* DynamicGateType::getGateContainer () {
    return &gateContainer;
}
