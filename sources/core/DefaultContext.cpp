
#include "../../headers/core/DefaultContext.h"
#include "../../headers/core/memories/RegGate.h"
#include "../../headers/core/GateType.h"

using namespace std;

DefaultContext::DefaultContext() {
    types["Reg"] = (GateType*) RegGate::TYPE;
}


bool DefaultContext::setType(string name, GateType* type) {
	bool alreadyExists = types.find (name) != types.end();
    types[name] = type;
    return alreadyExists;
}


bool DefaultContext::hasType(string name) {
    map<string,GateType*>::iterator it = types.find(name);
    return it != types.end();
}

GateType* DefaultContext::getType(string name) throw(GateTypeNotFoundException) {
    map<string,GateType*>::iterator it = types.find(name);
    if( it != types.end() ) {
        return types[name];
    }
    throw GateTypeNotFoundException(name);
}

set<string> DefaultContext::getTypeNames() {
    set<string> names;
    map<string,GateType*>::iterator it = types.begin();
    for( ; it != types.end(); ++it ) {
        names.insert( it->first );
    }
    return names;
}
