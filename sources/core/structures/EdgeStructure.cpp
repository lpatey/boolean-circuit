
#include "../../../headers/core/structures/EdgeStructure.h"
#include "../../../headers/core/structures/GateStructure.h"

using namespace std;

EdgeStructure::EdgeStructure(pair<string,GateStructure*> input) : input(input), outputs(), depth(0) {

}

EdgeStructure::EdgeStructure() : input(pair<string,GateStructure*>()), outputs(), depth(0) {
}

void EdgeStructure::addOutput(string name,GateStructure* output) throw(InvalidNameException) {
    outputs.push_back( pair<string, GateStructure*> (name, output ));
    output->addInput(name, this);
}

vector<pair<string,GateStructure*> >& EdgeStructure::getOutputs() {
    return outputs;
}

pair<string,GateStructure*>& EdgeStructure::getInput() {
    return input;
}

void EdgeStructure::setDepth(int depth) {
    this->depth = depth;
}

int EdgeStructure::getDepth() const {
    return depth;
}

bool EdgeStructure::hasInput() const {
    return input == pair<string,GateStructure*>();
}
