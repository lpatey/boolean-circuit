
#include "../../../headers/core/structures/GateStructure.h"
#include "../../../headers/core/GateType.h"
#include "../../../headers/core/structures/EdgeStructure.h"

#include <algorithm>
#include <vector>

using namespace std;

GateStructure::GateStructure( GateType* type ) :
    type(type), depth(depth), id(0), inputs(), outputs() {

    vector<string> outputNames = type->getOutputNames();
    for( int i=0; i<(int) outputNames.size(); i++ ) {
        string name = outputNames[i];
        outputs[name] = new EdgeStructure( pair<string, GateStructure*>(name, this));
    }
}

GateStructure::~GateStructure() {
    for( map<string,EdgeStructure*>::iterator it = outputs.begin(); it!=outputs.end(); ++it ) {
        delete it->second;
    }
}

void GateStructure::addInput( string name, EdgeStructure* input ) throw(InvalidNameException) {
    vector<string> inputNames = getType()->getInputNames();
    if( find(inputNames.begin(), inputNames.end(), name ) == inputNames.end() ) {
        throw InvalidNameException( name );
    }
    inputs[name] = input;
}


EdgeStructure* GateStructure::getOutput (string name) {
    return outputs[name];
}

EdgeStructure* GateStructure::getInput (string name) {
    return inputs[name];
}

std::map<std::string,EdgeStructure*>& GateStructure::getInputs() {
    return inputs;
}

std::map<std::string,EdgeStructure*>& GateStructure::getOutputs() {
    return outputs;
}

void GateStructure::setId(int id) {
    this->id = id;
}

int GateStructure::getId() const {
    return id;
}

void GateStructure::setDepth(int depth) {
    this->depth = depth;
}

int GateStructure::getDepth() const {
    return depth;
}

GateType* GateStructure::getType() const {
    return type;
}
