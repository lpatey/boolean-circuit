#include "../../../headers/core/memories/MemoryBit.h"

MemoryBit::MemoryBit() : value(false) {
}

MemoryBit::MemoryBit(bool iniValue) : value(iniValue) {
}


bool MemoryBit::getValue() const {
    return value;
}

void MemoryBit::setValue(bool value) {
    this->value = value;
}
