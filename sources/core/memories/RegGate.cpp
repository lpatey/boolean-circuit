
#include "../../../headers/core/memories/RegGate.h"
#include "../../../headers/core/memories/RegGateType.h"
#include "../../../headers/core/Edge.h"
#include <iostream>

using namespace std;

RegGateType* RegGate::TYPE = new RegGateType();

RegGate::RegGate() : Gate(RegGate::TYPE), MemoryBit(), memBits()  {
    memBits[""] = this;
}

RegGate::RegGate(bool firstValue) : Gate(RegGate::TYPE), MemoryBit(firstValue), memBits()  {
    memBits[""] = this;
}

GateType* RegGate::getType() const {
	return (GateType*) TYPE;
}

void RegGate::handleSignal() {
    //cout << endl;
   // cout << "Appel de handleSignal de registre" << endl;
}


void RegGate::tick(bool b) {
    //cout << endl;
  //  cout << "Appel de tick de registre" << endl;
  //  cout << (b ? "Cet appel demande de transmettre" : "Appel sans transmettre" ) << endl;

    if (b) {
        getOutputEdge("s")->setValue(getValue());
    }
    setValue(getInputEdge("a")->getValue());

  //  cout << "Input edge : " << getInputEdge("a")->getValue() << endl;
  //  cout << "Ma valeur dans registre : " << getValue() << endl;
  //  cout << "Output edge : " << getOutputEdge("s")->getValue() << endl;
}

map<string, MemoryBit*> RegGate::getMemoryBits() {
    return memBits;
}
