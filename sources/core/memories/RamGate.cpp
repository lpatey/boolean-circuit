#include <cmath> // Pour calculer 2^n
#include <sstream>

#include "../../../headers/core/memories/RamGate.h"
#include "../../../headers/core/memories/RamGateType.h"
#include "../../../headers/core/Edge.h"

using namespace std;

string getBinaryString (int n) {
    if (n == 1) {
        return "";
    }

    string strongBits = getBinaryString (n / 2);
    if (n % 2 == 0) {
        return (strongBits + "0");
    }
    return (strongBits + "1");
}

RamGate::RamGate(RamGateType* type) : Gate((GateType*) type), memBits(), type(type) {
    int sizeTot = (int) pow(2.0, type->getSize()), word = type->getWord();
    for (int i = 0 ; i < sizeTot ; i++) {
        for (int j = 0 ; j < word ; j++) {
            ostringstream oss;
            oss << getBinaryString(i) << "_" << j;
            memBits[oss.str()] = new MemoryBit ();
        }
    }
}

RamGate::~RamGate() {
    map<string, MemoryBit*>::iterator
        itMemBit = memBits.begin(),
        itMemBitEnd = memBits.end();
    for ( ; itMemBit != itMemBitEnd ; ++itMemBit) {
        delete itMemBit->second;
    }
}

GateType* RamGate::getType() const {
    return (GateType*) type;
}

void RamGate::handleSignal () {
    Gate::handleSignal ();
}

void RamGate::tick (bool b) {
    if (b) {
        // Obtention de l'adresse
        string adr = "";
        int size = type->getSize();
        for (int i = 0 ; i < size ; i++) {
            ostringstream oss;
            oss << "adr" << i;
            bool adr_value = getInputEdge(oss.str())->getValue();
            if (adr_value) {
                adr += "1";
            } else {
                adr += "0";
            }
        }

        bool wEnabled = getInputEdge("w_en")->getValue();

        int word = type->getWord();
        for (int i = 0 ; i < word ; i++) {
            ostringstream oss;
            oss << i;
            string iString = oss.str();
            string index = adr + "_" + iString;
            getOutputEdge("data_out" + iString)->setValue(memBits[index]->getValue());
            if (wEnabled) {
                memBits[index]->setValue(getInputEdge("data_in" + iString)->getValue());
            }
        }
    }
}

map<string, MemoryBit*> RamGate::getMemoryBits() {
    return memBits;
}

