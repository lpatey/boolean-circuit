#include <sstream>

#include "../../../headers/core/memories/RamGateType.h"

using namespace std;

// Pas des m�thodes, fonctions auxiliaires appel�es par le constructeur
// avant la cr�ation de l'objet
vector<string> computeInputNames (int size, int word) {
    vector<string> iNames;
    iNames.push_back("w_en");
    for (int i = 0 ; i < size ; i++) {
        ostringstream oss;
        oss << "adr" << i;
        iNames.push_back(oss.str());
    }
    for (int i = 0 ; i < word ; i++) {
        ostringstream oss;
        oss << "data_in" << i;
        iNames.push_back(oss.str());
    }
    return iNames;
}
vector<string> computeOutputNames (int word) {
    vector<string> oNames;
    for (int i = 0 ; i < word ; i++) {
        ostringstream oss;
        oss << "data_out" << i;
        oNames.push_back(oss.str());
    }
    return oNames;
}

RamGateType::RamGateType (int size, int word, Context* context) :
        GateType(computeInputNames(size, word), computeOutputNames(word), context),
        gateContainer(), size(size), word(word) {

    GateStructure* structure = gateContainer.addGateStructure(this);
    int iSize = inputNames.size(), oSize = outputNames.size();
    for (int i = 0 ; i < iSize ; i++) {
        gateContainer.addInput(inputNames[i], structure);
    }
    for (int i = 0 ; i < oSize ; i++) {
        gateContainer.setOutput(outputNames[i], structure);
    }
}

int RamGateType::getSize () {
    return size;
}

int RamGateType::getWord () {
    return word;
}

ConnectionType RamGateType::connectionType (string iName, string oName) {
    return MEMORY;
}

MetaType RamGateType::getMetaType () {
    return RAM;
}

GateContainer* RamGateType::getGateContainer () {
    return &gateContainer;
}

void RamGateType::compute() {
    GateType::compute();
}

int RamGateType::getMemoryBitsNumber() {
    return getSize();
}
