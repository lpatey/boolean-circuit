
#include "../../../headers/core/memories/RegGateType.h"
#include <vector>

using namespace std;

RegGateType::RegGateType () : GateType(vector<string>(1,"a"), vector<string>(1,"s"), (Context*) 0 ),
    gateContainer() {
	GateStructure* structure = gateContainer.addGateStructure(this);
	gateContainer.addInput("a", structure);
	gateContainer.setOutput("s", structure);

	compute();
}

ConnectionType RegGateType::connectionType (string input,string output) {
    return MEMORY;
}

MetaType RegGateType::getMetaType() {
	return REG;
}

GateContainer* RegGateType::getGateContainer () {
	return &gateContainer;
}

void RegGateType::compute () {
    GateType::compute();
}

int RegGateType::getMemoryBitsNumber() {
    return 1;
}
