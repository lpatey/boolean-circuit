
#include "../../headers/core/Clock.h"
#include "../../headers/core/Gate.h"
#include "../../headers/core/tools/GetTimeOfDay.h"
#include "../../headers/core/Edge.h"
#include "../../headers/core/GateType.h"
#include "../../headers/core/exceptions/TupleDimensionException.h"
#include <map>

using namespace std;

Clock::Clock (Gate* root) : running(false),
                        speed(1000),
                        root(root),
                        iterations(0),
                        listeners() {

}

int Clock::getSpeed () {
    return speed;
}

bool Clock::isRunning () {
    return running;
}

int Clock::getIterations () {
    return iterations;
}

void Clock::setSpeed (int speed) {
    this->speed = speed;
}

Gate* Clock::getRoot () {
    return root;
}

void Clock::start(Input& input, Output& output) {
    running = true;
    iterations = 0;
    timeval tv;
    int interval = speed/1000;
    long lastIteration = 0;

    vector<string> inputNames = root->getType()->getInputNames();
    int inputNamesSize = inputNames.size();
    map<string,Edge*> inputEdges;
    for( int i=0; i<inputNamesSize; i++ ) {
        Edge* edge = new Edge();
        inputEdges[ inputNames[i] ] = edge;
        edge->addOutput(inputNames[i], root);
    }

    gettimeofday(&tv, NULL);
    lastIteration = tv.tv_usec;
    while(input.hasNext()) {
        iterations++;

        {
            map<string,bool> values = input.getNext();
            int valuesSize = values.size();
            if( valuesSize != inputNamesSize ) {
                throw TupleDimensionException( "input values", "input names", "input" );
            }

            map<string,bool>::iterator it = values.begin();
            for( ; it != values.end(); ++it ) {
                inputEdges[ it->first ]->setValue( it->second );
            }
        }

        root->tick(true);
        root->tick(false);

        iterations++;

        {
            map<string,bool> outValues;
            map<string,Edge*> outEdges = root->getOutputEdges();
            map<string,Edge*>::iterator it = outEdges.begin();

            for( ; it != outEdges.end(); ++it ) {
                outValues[it->first] = it->second->getValue();
            }

            output.setNext (outValues);
        }

        while(true) {
            gettimeofday(&tv, NULL);
            if( tv.tv_usec - lastIteration >= interval ) {
                lastIteration = tv.tv_usec;
                break;
            }
        }
    }

    for( unsigned int i=0; i<inputNames.size(); i++ ) {
        delete inputEdges[ inputNames[i] ];
    }
}

