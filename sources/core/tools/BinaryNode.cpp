
#include "../../../headers/core/tools/BinaryNode.h"
#include "../../../headers/core/tools/Leaf.h"
#include <iostream>

using namespace std;

BinaryNode::BinaryNode(string name): name(name), left(new Leaf()), right(new Leaf()) {

}

BinaryNode::BinaryNode(const BinaryNode& from) {
    name = from.name;
    left = from.left->clone();
    right = from.right->clone();
}

BinaryNode& BinaryNode::operator=(const BinaryNode& from) {
    name = from.name;
    left = from.left->clone();
    right = from.right->clone();
    return *this;
}

BinaryNode::~BinaryNode() {
    delete left;
    delete right;
}

INode* BinaryNode::clone() const {
    return (INode*) new BinaryNode( *this );
}

INode* BinaryNode::get(bool dir) {
    return dir ? right : left;
}

void BinaryNode::setLeaf(bool dir, map<string, bool> values) {
    if( dir ) {
        delete right;
        right = new Leaf(values);
    }
    else {
        delete left;
        left = new Leaf(values);
    }
}

void BinaryNode::addNode(bool dir, string name) {
    if( dir ) {
        delete right;
        right = new BinaryNode(name);
    }
    else {
        delete left;
        left = new BinaryNode(name);
    }
}

bool BinaryNode::isLeaf() const {
    return false;
}

bool BinaryNode::isNull() const {
    return false;
}

map<string,bool>& BinaryNode::getValue(map<string,bool> path) {
    bool dir = path[name];
    return get(dir)->getValue(path);
}

bool BinaryNode::setValue(map<string, bool> path, map<string,bool> values, bool strict) throw(BinaryTreeException) {

    if( path.find( name ) == path.end() ) {
        path[name] = true;
        setValue(path, values);
        path[name] = false;
        setValue(path, values);
        return false;
    }

    bool dir = path[name];
    path.erase(name);

    if(path.empty()) {
        if(! get(dir)->isNull()) {
            if( strict ) {
                throw BinaryTreeException( "Already set" );
            }
            else {
                get(dir)->setValue(path,values);
                return false;
            }
        }
        if(get(!dir)->isLeaf() &&!get(!dir)->isNull() && get(!dir)->getValue(path) == values) {
            return true;
        }
        setLeaf(dir,values);
        return false;
    }

    if(get(dir)->isNull()) {
        addNode(dir, (*path.begin()).first);
        if( get(dir)->setValue(path, values) ) {
            if(get(!dir)->isLeaf() &&!get(!dir)->isNull() && get(!dir)->getValue(path) == values) {
                return true;
            }
            setLeaf(dir,values);
        }
        return false;
    }

    if(get(dir)->isLeaf()) {
        if( strict ) {
            throw BinaryTreeException("Leaf already set");
        }
        else {
            return false;
        }
    }

    if(get(dir)->setValue(path, values)) {
        setLeaf(dir, values);
        return (get(!dir)->isLeaf() && get(!dir)->getValue(path) == values);
    }

    return false;
}

void BinaryNode::copyExtract(set<string> variablesInput, set<string> variablesOutput,
		map<string, bool> ancestors, BinaryTree* destination) {
	if (variablesInput.count(name) == 0) {
		if (! right->isNull())
			right->copyExtract(variablesInput, variablesOutput, ancestors, destination);
		if (! left->isNull())
			left->copyExtract(variablesInput, variablesOutput, ancestors, destination);
	}
	if (! right->isNull()) {
		ancestors[name] = true;
		right->copyExtract(variablesInput, variablesOutput, ancestors, destination);
	}
	if (! left->isNull()) {
		ancestors[name] = false;
		left->copyExtract(variablesInput, variablesOutput, ancestors, destination);
	}
}

bool BinaryNode::isComplete () {
    return (right->isComplete() && left->isComplete());
}

string BinaryNode::toString (string ancestors) {
    string subS = ancestors + ", " + name + " = ";
    string subSR = subS + "1", subSL = subS + "0";
    return left->toString(subSL) + right->toString(subSR);
}
