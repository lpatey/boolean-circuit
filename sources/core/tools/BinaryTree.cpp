
#include "../../../headers/core/tools/BinaryTree.h"
#include "../../../headers/core/tools/Leaf.h"
#include "../../../headers/core/tools/BinaryNode.h"

using namespace std;

BinaryTree::BinaryTree() {
    root = new Leaf();
}

BinaryTree::~BinaryTree() {
    delete root;
}

BinaryTree::BinaryTree(const BinaryTree& from) {
    root = from.root->clone();
}

BinaryTree& BinaryTree::operator=(const BinaryTree& from) {
    root = from.root->clone();
    return *this;
}

map<string, bool>& BinaryTree::getValue (map<string, bool> path) {
    return root->getValue(path);
}

void BinaryTree::setValue (map<string, bool> path, map<string, bool> values, bool strict)
	throw(BinaryTreeException) {

    if(root->isNull()) {
        delete root;
        if( path.empty() ) {
            root = new Leaf(values);
            return;
        }
        root = new BinaryNode( (*path.begin()).first);
    }

    if (root->setValue(path, values)) {
        delete root;
        root = new Leaf(values);
    }
}

void BinaryTree::copyExtract(set<string> variablesInput,
		set<string> variablesOutput, BinaryTree* destination) {
	if (root->isNull()) {
		return ;
	}

	if (root->isLeaf()) {
		map<string, bool> emptyPath;
		destination->setValue(emptyPath, root->getValue(emptyPath));
		return ;
	}

	root->copyExtract(variablesInput, variablesOutput, map<string, bool> (), destination);
}

bool BinaryTree::isComplete () {
    return root->isComplete();
}

string BinaryTree::toString () {
    return root->toString("");
}
