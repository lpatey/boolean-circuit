
#ifdef WIN32

#include "../../../headers/core/tools/GetTimeOfDay.h"

#include <time.h>
#include <windows.h>

LARGE_INTEGER GetFrequency() {
      LARGE_INTEGER freq;
      QueryPerformanceFrequency(&freq);
      return freq;
}

// Le deuxi�me argument est ignor� pour l'instant, cette fonction n'est utilis�e que pour des calculs de temps �coul�s
// pour des raisons de portabilit�
int gettimeofday(struct timeval *tv, struct timezone *tz) {
      LARGE_INTEGER tick, freq;
      freq = GetFrequency();
      QueryPerformanceCounter(&tick);
      tv->tv_sec = tick.QuadPart / freq.QuadPart;
      tv->tv_usec = ((1000000 * tick.QuadPart) / freq.QuadPart) - tv->tv_sec;
      return 0;
}

#endif

