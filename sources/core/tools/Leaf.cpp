#include<sstream>

#include "../../../headers/core/tools/Leaf.h"

using namespace std;

Leaf::Leaf() : values() {
}

Leaf::~Leaf() {
}

INode* Leaf::clone() const {
    return (INode*) new Leaf(*this);
}

Leaf::Leaf (map<string,bool> values) : values(values) {
}

map<string,bool>& Leaf::getValue (map<string, bool> path) {
    return values;
}

bool Leaf::setValue(map<string, bool> path, map<string,bool> values, bool strict) throw(BinaryTreeException) {
    if(strict) {
        throw BinaryTreeException ("Leaf : not mutable");
    }
    return false;
}

bool Leaf::isLeaf() const {
    return true;
}

bool Leaf::isNull () const {
    return values.empty();
}

void Leaf::copyExtract (set<string> variablesInput, set<string> variablesOutput,
		map<string, bool> ancestors, BinaryTree* destination) {
	map<string, bool> extractedValues;
	map<string, bool>::iterator
		itValues = values.begin(),
		itValuesEnd = values.end();
	for ( ; itValues != itValuesEnd ; ++itValues) {
		if (variablesOutput.count(itValues->first) != 0) {
			extractedValues[itValues->first] = itValues->second;
		}
	}
	destination->setValue(ancestors, extractedValues);
}

bool Leaf::isComplete () {
    return (! isNull());
}

string Leaf::toString (string ancestors) {
    string result;
    if (isNull()) {
        result = "?";
    } else {
        ostringstream oss;
        map<string, bool>::iterator
            itValue = values.begin(),
            itValueEnd = values.end();
        for ( ; itValue != itValueEnd ; ++itValue) {
            oss << " ; " << itValue->first << " = " << itValue->second;
        }
        result = oss.str();
    }
    return (ancestors + " -> " + result + "\n");
}
