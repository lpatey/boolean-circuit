// Options à fixer à la compilation
//define USE_BOOST_PROGRAM_OPTIONS

#include "../headers/parser/FileContext.h"
#include "../headers/Config.h"
#include "../headers/core/Gate.h"
#include "../headers/core/Clock.h"
#include "../headers/core/GateType.h"
#include "../headers/parser/FileInput.h"
#include "../headers/parser/StandardOutput.h"

#include <iostream>

using namespace std;

int main(int argc, char* argv[])
{

    Config* config = Config::init(argc, argv);

    if( !config->compute() ) {
        return 0;
    }

    try {

        string filePath = config->getFilePath();
        int clockSpeed = config->getClockSpeed();

        FileContext* context = FileContext::getContext (filePath);

        if (config->isComplete()) {

            string rootName = config->getSimulatedGate();
            string inputFilePath = config->getInputFilePath();
            GateType* rootType = context->getType(rootName);

            Gate* root = Gate::create(rootType);

            FileInput input (inputFilePath);
            StandardOutput output;

            Clock clock(root);
            clock.setSpeed(clockSpeed);
            clock.start (input, output);

            delete root;
        }
    }
    catch( GateLibException e) {
        cerr << "Error : " << e.getMessage() << endl;
    }

    return 0;
}
